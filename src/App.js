import React, {Component} from 'react';
import './App.css';
import Home from './Common/Component/Home/Home';
import { connect } from "react-redux";
import './assets/css/bootstrap.min.css'
import './assets/css/font-awesome.min.css'
import './assets/css/style.css'
import {ProtectedRoute} from './Common/libs/protected.route';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ErrorPage from './Common/Component/ErrorPage/ErrorPage';
import ListingGrid from './Common/Component/ListingGrid/ListingGrid';
import ListingInfo from './Common/Component/ListingInfo/ListingInfo';
import DashBoardHome from './Common/Component/Dashboard/Home/Home';
import Loader from './Loader/Loader';
import AboutUs from './Common/Component/AboutUs/AboutUs';
import HowItWork from './Common/Component/HowItWork/HowItWork';
import ContactUs from './Common/Component/ContactUs/ContactUs';
import ManageCategory from './Common/Component/Dashboard/ManageCategory/ManageCategory';
import ManagePlans from './Common/Component/Dashboard/ManagePlans/ManagePlans';
import MyListing from './Common/Component/Dashboard/MyListing/MyListing';
import AddNewList from './Common/Component/Dashboard/AddNewListing/AddNewList';
import ManageUser from './Common/Component/Dashboard/ManageUser/ManageUser';
import ChangePassword from './Common/Component/Dashboard/ChangePassword/ChangePassword';
import MyProfile from './Common/Component/Dashboard/MyProfile/MyProfile';
import EditProfile from './Common/Component/Dashboard/EditProfile/EditProfile';
import ManageListing from './Common/Component/Dashboard/ManageListing/ManageListing'
import ManageQueryAdmin from './Common/Component/Dashboard/ManageQueryAdmin/ManageQueryAdmin';
import ManageQueryBusiness from './Common/Component/Dashboard/ManageQueryBusiness/ManageQueryBusiness'
class App extends Component{
  constructor(props){
    super(props)
    
  }
  render(){
    let props = this.props;
    let loader = props.LOADER
    
  return (
    <div className="App">
      
      <Router>
      {loader && loader.show ? <Loader/>: null}
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/aboutus" component={AboutUs}/>
        <Route exact path="/howitwork" component={HowItWork}/>
        <Route exact path="/contactus" component={ContactUs}/>
        <Route exact path="/listinggrid" component={ListingGrid}/>
        <Route exact path="/listinginfo" component={ListingInfo}/>
        <ProtectedRoute exact path="/dashboard" component={DashBoardHome}/>
        <ProtectedRoute exact path="/dashboard/managecategory" component={ManageCategory}/>
        <ProtectedRoute exact path="/dashboard/mylisting" component={MyListing}/>
        <ProtectedRoute exact path="/dashboard/addnewlist" component={AddNewList}/>
        <ProtectedRoute exact path="/dashboard/manageusers/business" component={ManageUser} />
        <ProtectedRoute exact path="/dashboard/manageusers/user" component={ManageUser}/>
        <ProtectedRoute exact path="/dashboard/changepassword" component={ChangePassword}/>
        <ProtectedRoute exact path="/dashboard/manageplans" component={ManagePlans}/>
        <ProtectedRoute exact path="/dashboard/myprofile" component={MyProfile}/>
        <ProtectedRoute exact path="/dashboard/editprofile" component={EditProfile}/>
        <ProtectedRoute exact path="/dashboard/managelisting" component={ManageListing}/>
        <ProtectedRoute exact path="/dashboard/managequeryBusiness" component={ManageQueryBusiness}/>
        <ProtectedRoute exact path="/dashboard/managequeries" component={ManageQueryAdmin}/>
        <Route component={ErrorPage}/>
        </Switch>
      </Router>
           
    </div>
  );
  }
}
const mapStateToProps = (state) => {
  return {
    LOADER:state.loader
  }
}
export default connect(mapStateToProps, null)(App);
