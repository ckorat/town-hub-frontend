import axios from "axios";
let profileService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/profile-service";
    let getProfileByRole = function (param) {
        let api_url = `${basePath}/profiles/role/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateStatus = function(param){
        let api_url = `${basePath}/profiles/${param.profileId}/${param.status}`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateProfileImage = function(param){
        console.log(param)
        let api_url = `${basePath}/profiles/${param.id}/images/`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateProfile = function(param){
        console.log(param)
        let api_url = `${basePath}/profiles/${param.id}/`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updatePassword = function(param){
        let api_url = `${basePath}/profiles/`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let getProfileByProfileId = function (param) {
        let api_url = `${basePath}/profiles/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return {
       getProfileByRole,
       updateStatus,
       updatePassword,
       getProfileByProfileId,
       updateProfileImage,
       updateProfile
    }
})();

export default profileService;