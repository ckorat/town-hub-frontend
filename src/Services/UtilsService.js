import axios from "axios";
let UtilsService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/utils-service";
    let getAddressById = function(param){
        let api_url = `${basePath}/api/v1/addresses/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let saveAddress = function(param){
        let api_url = `${basePath}/api/v1/addresses/`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let getContactById = function(param){
        let api_url = `${basePath}/api/v1/contact/${param}`;

        return new Promise((resolve, reject) => {
            axios.get(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let saveContact = function(param){
        let api_url = `${basePath}/api/v1/contact/`;

        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateAddress = function(param){
        let api_url = `${basePath}/api/v1/addresses/${param.id}`;

        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateContact = function(param){
        let api_url = `${basePath}/api/v1/contact/${param.id}`;

        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return { 
        getAddressById,
        getContactById,
        updateAddress,
        updateContact,
        saveAddress,
        saveContact
    }
})();
export default UtilsService;