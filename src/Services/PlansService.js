import axios from "axios";
let PlansService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/listing-service";
    let getAllPlans = function () {
        let api_url = `${basePath}/api/v1/plans/`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }

    let addPlans = function(param){
        let api_url = `${basePath}/api/v1/plans/`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateStatus = function(param){
        let api_url = `${basePath}/api/v1/plans/${param.planId}/${param.status}`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updatePlans = function(param){
        let api_url = `${basePath}/api/v1/plans/${param.planId}`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return { 
        addPlans,
        getAllPlans,
        updateStatus,
        updatePlans
    }
})();
export default PlansService;