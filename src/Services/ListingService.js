import axios from "axios";
let ListingService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/listing-service";

    let addListing = function(param){
        let api_url = `${basePath}/api/v1/listing/`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let addListingTime = function(param){
        let api_url = `${basePath}/api/v1/listing/addListingTime`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    
    let getAllListing = function(){
        let api_url = `${basePath}/api/v1/listing/`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }

    let getAllVerifiedListing = function(){
        let api_url = `${basePath}/api/v1/listing/verify`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }

    let getAllListingByBusiness = function(param){
        let api_url = `${basePath}/api/v1/listing/business/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }

    let getListingTime = function(param){
        let api_url = `${basePath}/api/v1/listing/${param}`;
        return new Promise((resolve,param,reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }

    let updateStatus = function(param){
        let api_url = `${basePath}/api/v1/listing/${param.field}/${param.id}/${param.status}`;
        return new Promise((resolve,reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return { 
        addListing,
        addListingTime,
        getAllListing,
        getListingTime,
        getAllListingByBusiness,
        updateStatus,
        getAllVerifiedListing
    }
})();
export default ListingService;