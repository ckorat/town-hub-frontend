import axios from "axios";
let QueryService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/utils-service";
    let addQuery = function(param){
        let api_url = `${basePath}/api/v1/queries/`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let getQuery = function(){
        let api_url = `${basePath}/api/v1/queries/`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let getQueryByBusinessId = function(param){
        let api_url = `${basePath}/api/v1/queries/businessQueries/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return { 
        addQuery,
        getQuery,
        getQueryByBusinessId
    }
})();
export default QueryService;