import axios from "axios";
let categoryService = (function () {
    const headers = {
        'Content-Type': 'application/json',
    }
    let basePath="http://localhost:8080/utils-service";
    let getAllCategories = function () {
        let api_url = `${basePath}/api/v1/categories/`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let getCategoryCount = function (param) {
        let api_url = `${basePath}/api/v1/categories/${param}`;
        return new Promise((resolve, reject) => {
            axios.get(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let addCategory = function(param){
        let api_url = `${basePath}/api/v1/categories/`;
        return new Promise((resolve, reject) => {
            axios.post(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateStatus = function(param){
        let api_url = `${basePath}/api/v1/categories/${param.categoryId}/${param.status}`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let updateCategory = function(param){
        let api_url = `${basePath}/api/v1/categories/${param.categoryId}`;
        return new Promise((resolve, reject) => {
            axios.put(api_url,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    return {
       getAllCategories,
       addCategory,
       updateStatus,
       updateCategory,
       getCategoryCount
    }
})();

export default categoryService;