import axios from "axios";
let LoginService = (function () {
    const headers = {
        'Content-Type': 'application/json',
        
    }
    let basePath="http://localhost:8080";
    let authetication = function (param) {
        console.log(param)
        let api_url = `${basePath}/auth/`;
        return new Promise((resolve ,reject) => {
            axios.post(api_url ,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    let signup = function (param) {
        console.log(param)
        let api_url = `${basePath}/profile-service/profiles/`;
        return new Promise((resolve ,reject) => {
            axios.post(api_url ,param,headers)
                .then(
                    (response) => resolve(response)
                )
                .catch(
                    (error) => reject(error)
                )
        })
    }
    axios.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        let res = error.response;
        return Promise.reject(res);
    });
    axios.interceptors.request.use((request)=>{
        request.headers.Authorization=sessionStorage.getItem("authToken")
        return request;
    }, function(error){
        return Promise.reject(error);
    });
    return {
       authetication,
       signup
    }
})();

export default LoginService;