import React from "react";
import { connect } from "react-redux";
import { loader } from "../Redux/Actions/index";
import loaderImage from './abc.gif';
import "./loader.css";
class Loader extends React.Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return (
            <div className="sample_loader">
                <img alt="Sample_loader" src={loaderImage}/>
            </div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader: (payload) => {
            dispatch(loader(payload))
        }
    }
}

const mapStateToProps = (state) => {
    return {
        LOADER: state.loader
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Loader);