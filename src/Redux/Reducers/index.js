import { combineReducers } from "redux";
import loader from "./LoaderReducers";
const rootReducers = combineReducers({
    loader
})

export default rootReducers;