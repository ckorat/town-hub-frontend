import ACTIONS from "../Actions";
export default function loaderReducer(state = {show:false},action){
    switch(action.type){
        case ACTIONS.LOADER:
            return {...state,...action.payload};
        default:
            return state;
    }
}