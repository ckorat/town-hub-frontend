const actions = {
    LOADER:'LOADER'
}
export function loader(payload = {show:false}){
    return {
        type:actions.LOADER,
        payload:payload
    }
}
export default actions;