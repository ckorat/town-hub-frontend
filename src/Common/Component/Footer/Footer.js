import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './footer.css';
class Footer extends Component{
    render(){
        return(
              <div>      
          <footer id="footer" className="secondary-bg">
	<div className="container">
    	<div className="row">
        	
            
            <div className="col-md-4">
            	<div className="footer_widgets">
                	<h5>Quick Links</h5>
                    <div className="footer_nav">
                    	<ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/howitwork">How it Work</Link></li>
                            <li><Link to="/listinggrid">Listing</Link></li>
                            <li><Link to="/aboutus">About Us</Link></li>
                            <li><Link to="/contactus">Contact Us</Link></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
            	
            </div>
            <div className="col-md-4">
            	<div className="footer_widgets">
                	<h5>Our Newsletter</h5>
                    <div className="newsletter_wrap">
                    	<form action="#" method="get">
                        	<input type="email" className="form-control" placeholder="Enter Email Address"/>
                            <input type="submit" value="subscribe" className="btn"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div className="footer_bottom">
    	<div className="container">
        	<p>Copyright &copy; 2020 TownHub. All Rights Reserved</p>
        </div>
    </div>
</footer>
        </div>
        
        );
    }
}
export default Footer;