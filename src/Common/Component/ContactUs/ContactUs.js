import React, { Component } from "react";
import SimpleReactValidator from 'simple-react-validator';
import { loader } from "../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import queryService from '../../../Services/QueryService'
import "./contactUs.css";

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName:'',
      lastName:'',
      phoneNumber:'',
      email:'',
      title:'',
      message:''
    }
    this.validator = new SimpleReactValidator({autoForceUpdate: this});
  }

  handleChange = event => {
		let change = {}
		change[event.target.name] = event.target.value
		this.setState(change)
  };
  handleSubmitQuery = (event) =>{
    event.preventDefault();
    this.props.loader({show:true});
    if (this.validator.allValid()) {
      const data={
        firstName: this.state.firstName,
        lastName:this.state.lastName,
        phoneNumber:this.state.phoneNumber,
        email:this.state.email,
        title:this.state.title,
        message:this.state.message
      }
      queryService.addQuery(data).then(
        (response)=>{
          this.props.loader({show:false});
          console.log(response)
          this.props.history.push("/");
          swal("Success..", "Your query has been successfully registered.", "success");
        },
        (error)=>{
          this.props.loader({show:false});
          console.log(error);
          swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
        }
        )
    }
    else{
      this.props.loader({show:false});
      this.validator.showMessages();
      this.forceUpdate();
    }
  }
  render() {
    return (
      <div>
        <Header />
        <section id="contactus_bg" className="parallex-bg contactus_bg">
          <div className="container">
            <div className="white-text text-center div_zindex">
              <h1>Contact Us </h1>
            </div>
          </div>
          <div className="dark-overlay"></div>
        </section>
        <section id="inner_pages">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="office_info_box">
                  <div className="info_icon">
                    <i className="fa fa-home"></i>
                  </div>
                  <h4>Office Address</h4>
                  <p>
                    7th Floor, Lunkad Sky Vista, Viman Nagar, Pune, Maharashtra
                    411014
                  </p>
                </div>
              </div>

              <div className="col-md-4">
                <div className="office_info_box">
                  <div className="info_icon">
                    <i className="fa fa-phone"></i>
                  </div>
                  <h4>Phone Number</h4>
                  <p>
                    020 6685 3600
                    <br />
                    020 6685 3600
                  </p>
                </div>
              </div>

              <div className="col-md-4">
                <div className="office_info_box">
                  <div className="info_icon">
                    <i className="fa fa-envelope-o"></i>
                  </div>
                  <h4>Email Address</h4>
                  <p>
                    <a href="mailto:info@bbdsoftware.com">
                      info@bbdsoftware.com
                    </a>{" "}
                    <br />
                    <a href="mailto:info@example.com">info@bbd.co.za</a>{" "}
                  </p>
                </div>
              </div>
            </div>
            
            <h4>Let's get in touch</h4>
            <div className="row">
              <div className="col-md-6">
                <div className="contact_form">
                  <form onSubmit={this.handleSubmitQuery}>
                    <div className="row">
                      <div className="form-group col-sm-6">
                        <input type="text" className="form-control" placeholder="First Name" name = "firstName" value={this.state.firstName} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('firstName') }></input>
                        <div>
                            <span className="has-error">{this.validator.message('firstName', this.state.firstName, 'required|min:3')}</span>
                      </div>
                      </div>
                      <div className="form-group col-sm-6">
                        <input
                          type="text"className="form-control"placeholder="Last Name"name = "lastName" value={this.state.lastName} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('lastName') }></input>
                          <div>
                              <span className="has-error">{this.validator.message('lastName', this.state.firstName, 'required|min:3')}</span>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                    <input type="text" className="form-control" placeholder="Phone Number" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('phoneNumber') }></input>
                    <div>
                <span className="has-error">{this.validator.message('phoneNumber', this.state.phoneNumber, 'required|min:10|max:10|phone')}</span>
            </div>
                    </div>
                    <div className="form-group">
                      <input
                        type="text"className="form-control"placeholder="Email Address" name="email" value={this.state.email} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('email')}></input>
                        <div>
                <span className="has-error">{this.validator.message('email', this.state.email, 'required|email')}</span>
            </div>
                    </div>
                    <div className="form-group">
                      <input
                        type="text"className="form-control"placeholder="Title" name="title" value={this.state.title} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('title')}></input>
                        <div>
                        <span className="has-error">{this.validator.message('title', this.state.title, 'required')}</span>
                        </div>
                    </div>
                    <div className="form-group">
                      <textarea className="form-control" placeholder="Message" name="message" value={this.state.message} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('message')}></textarea>
                      <div>
                      <span className="has-error">{this.validator.message('message', this.state.message, 'required|max:100')}</span>
                      </div>
                    </div>
                    <button className="btn" onClick={this.handleSubmitQuery}>Submit</button>    
                  </form>
                </div>
              </div>

              <div class="col-md-6">
            	<div class="img_wrap">
                	<img src={ require('./contact.png') } alt="image"></img>
                </div>
            </div>
            </div>
            </div>
        </section>
        <Footer />
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
      loader:(payload)=>{
          dispatch(loader(payload));
      }
  }
}
export default connect(null, mapDispatchToProps)(ContactUs);