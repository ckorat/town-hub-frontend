import React, {Component} from 'react';
import SimpleReactValidator from 'simple-react-validator';
import { loader } from "../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom'; 
import loginService from '../../../Services/LoginService'
import './login.css';


class Login extends Component{
    constructor(props){
        super(props);
        this.state={
			isSignup:false,
			username:'',
			password:'',
			firstName:'',
			lastName:'',
			signusername:'',
			email:'',
			signPassword:'',
			cnfPassword:'',
			isBusiness:'user',
			cnfpasswordError:false,
			isUsernameExist:false,
			isLogged:false,
			isBlocked:false,
		}
		
		this.handleSignIn=this.handleSignIn.bind(this);
		this.validator = new SimpleReactValidator({autoForceUpdate: this});
		this.validator2 = new SimpleReactValidator({autoForceUpdate: this});
		this.checkPassword=this.checkPassword.bind(this);
		this.handleCheckBox=this.handleCheckBox.bind(this);
	}
    handleSignIn(){
        this.setState(prevState => ({
            isSignup: !prevState.isSignup
          }));
	}
	handleCheckBox(event){
		if(event.target.checked){
			this.setState({
				isBusiness:'business'
			})
		}else{
			this.setState({
				isBusiness:'user'
			})
		}
	}
	handleChange = event => {
		let change = {}
		change[event.target.name] = event.target.value
		this.setState(change)
  };
  handleSignUp= (event)=>{
	
		  event.preventDefault();
		  this.props.loader({show:true});
		  if (this.validator2.allValid()) {
			  if(this.state.signPassword===this.state.cnfPassword){
				  const data={
					  userName:this.state.signusername,
					  password:this.state.signPassword,
					  profileActive:true,
					  firstName:this.state.firstName,
					  lastName:this.state.lastName,
					  role:this.state.isBusiness,
					  address:{

					  },
					  contact:{
						email:this.state.email,
						phoneNumber:this.state.phonenumber
					  }
				  }
				  loginService.signup(data).then(
					(response)=>{
						this.props.loader({show:false});
						console.log(response)
						swal("Registered", "Thank You For Connecting With Us. Login to continue..", "success");
						this.setState({
							isSignup:false,
							isUsernameExist:false
						})
					},
					(error)=>{
						if(error.status===409){
							this.props.loader({show:false});
							console.log(error)
							this.setState({
								isUsernameExist:true
							})
						}
						else{
							this.setState({
								isUsernameExist:false
							})
							this.props.loader({show:false});
							console.log(error);
							swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
						}
						
					}
				  )
			  }else{
				  this.setState({
					cnfpasswordError:true
				  })
				  this.props.loader({show:false});
			  }
			
			
		}
		  else{
			this.props.loader({show:false});
			this.validator2.showMessages();
        	this.forceUpdate();
		  }
  	}
  onSubmitLogin = (event)=>{
	event.preventDefault();
	
	if (this.validator.allValid()) {
		this.props.loader({show:true});
		const data={
			username:this.state.username,
			password:this.state.password
		}
		loginService.authetication(data).then(
			(response)=>{
				console.log(response)
				if(!response.data.status){
					this.setState({
						isBlocked:true
					})
					this.props.loader({show:false});
				}else{
					console.log(response.headers.authorization);
					sessionStorage.setItem("authToken",response.headers.authorization);
					sessionStorage.setItem("loginState",JSON.stringify(response.data));
					this.props.loader({show:false});
					this.setState({
						isLogged:true
					},this.props.checkLogin)
					
					swal("Welecome Back..", "Your Login Succesfully.", "success");
				}
				
			},
			(error)=>{
				console.log(error)
				if(error?.status===401){
					swal("Oops..", "Your Login Failed. Please Check Your UserName Or Password..", "error");
				}else{
					
					swal("Oops..", "Something Went Wrong.. Please Try Again Later..", "error");
				}
				this.props.loader({show:false});
				throw error
			}
		)
	}else{
		this.validator.showMessages();
        this.forceUpdate();
	}
  }
  checkPassword(event){
	  this.handleChange(event)
	  console.log(event)
	  if(!this.validator2.fieldValid('confirmPassword')){
			this.validator2.showMessageFor('confirmPassword')
			this.forceUpdate();
	  }
	  if(this.state.signPassword!==this.state.cnfPassword){
		this.setState({
			cnfpasswordError:true
		})
	}else{
		this.setState({
			cnfpasswordError:false
		})
	}
  }
    render(){
		if(this.state.isLogged){
			return <Redirect to="/"/>
		}
        return(
            <div className="login">
         
            <div className={!this.state.isSignup? "container2": "container2 right-panel-active"} id="container2">
            
	<div className="form-container sign-up-container">
		<form onSubmit={this.handleSignUp}>
			<h2>Create Account</h2>
			
			<input type="text" placeholder="First Name" name="firstName" value={this.state.firstName} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('firstName') }/>
			<div>
                <span className="has-error">{this.validator2.message('firstName', this.state.firstName, 'required|min:3')}</span>
            </div>
			<input type="text" placeholder="Last Name" name="lastName" value={this.state.lastName} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('lastName') }/>
			<div>
                <span className="has-error">{this.validator2.message('lastName', this.state.lastName, 'required|min:3')}</span>
            </div>
			<input type="email" placeholder="Email" name="email" value={this.state.email} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('email') }/>
			<div>
                <span className="has-error">{this.validator2.message('email', this.state.email, 'required|email')}</span>
            </div>
			<input type="text" placeholder="Username" name="signusername" value={this.state.signusername} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('username') }/>
			<div>
                <span className="has-error">{this.validator2.message('username', this.state.signusername, 'required|min:3')}</span>
            </div>
			{this.state.isUsernameExist?<div>
                <span className="has-error">Username Is Already Exist Please select other Username.</span>
            </div>:null}
			
			<input type="password" placeholder="Password" name="signPassword" value={this.state.signPassword} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('password') }/>
			<div>
                <span className="has-error">{this.validator2.message('password', this.state.signPassword, 'required|min:6|max:15')}</span>
            </div>
			<input type="password" placeholder="Confirm Password" name="cnfPassword" value={this.state.cnfPassword} onChange={this.handleChange} onBlur={this.checkPassword}/>
			{this.state.cnfpasswordError?<div>
                <span className="has-error">Confirm Password Should Be Same.</span>
            </div>:null}
			<div>
                <span className="has-error">{this.validator2.message('confirmPassword', this.state.cnfPassword, 'required|min:6|max:15')}</span>
            </div>
            <input type="text"  placeholder="Phone Number" name="phonenumber" value={this.state.phonenumber} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('phonenumber') }/>
			<div>
                <span className="has-error">{this.validator2.message('phonenumber', this.state.phonenumber, 'required|min:10|max:10|phone')}</span>
            </div>
			<p>
    				<input type="checkbox" onClick={this.handleCheckBox} id="test1" />
    				<label htmlFor="test1">Register As A Business.</label>
  			</p>
  			<button onClick={this.handleSignUp}>Sign Up</button>
		</form>
	</div>
	<div className="form-container sign-in-container">
		<form onSubmit={this.onSubmitLogin}> 
			<h2 >Sign in</h2>
			<input type="text" name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('username')}/>
			<div>
                <span className="has-error">{this.validator.message('username', this.state.username, 'required|min:3')}</span>
            </div>
			<input type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('password')}/>
			<div>
                <span className="has-error">{this.validator.message('password', this.state.password, 'required|min:5|max:15')}</span>
            </div>
			{this.state.isBlocked?
			<div>
			<span className="has-error">You have been blocked by the admin.. Please Contact Your Adminstration.. Thank You.</span>
		</div>:null	
		}
			
			<button onClick={this.onSubmitLogin}>Sign In</button>
		</form>
	</div>
	<div className="overlay-container ">
		<div className="overlay">
			<div className="overlay-panel overlay-left">
				<h1 className="login2h1">Welcome Back!</h1>
				<p>To keep connected with us please login with your personal info</p>
				<button className="ghost" id="signIn" onClick={this.handleSignIn}>Sign In</button>
			</div>
			<div className="overlay-panel overlay-right">
				<h1 className="login2h1">Hello, Friend!</h1>
				<p>Enter your personal details and start journey with us</p>
				<button className="ghost" id="signUp" onClick={this.handleSignIn}>Sign Up</button>
                
			</div>
		</div>
	</div>
</div>
</div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(Login);