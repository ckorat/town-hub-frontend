import React, {Component} from 'react'
import './listingpackage.css';

class Package extends Component{
    constructor(props){
        super(props);
    }
    render(){
        
        return(
            <div className="col-md-4">
            	<div className="pricing_wrap">
                	<div className="pricing_header">
        <h2>{this.props.data.planName}</h2>
                        <p>{this.props.data.planName}</p>
                    </div>
                    <div className="plan_info">
        <div className="plan_price">{this.props.data.planAmount==="0"?"Free":"₹ "+this.props.data.planAmount}</div>
                        <ul>
                        	<li><span>{this.props.data.planListingLimit}</span> Listing</li>
                            <li><span>{this.props.data.planDuration} Days </span> Availability</li>
                            <li><span>{this.props.data.contactShow?"Show":"No"} </span>Contact Detail</li>
                            <li><span>{this.props.data.reviewShow?"Show":"No"} </span> Review Detail</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default Package