import React, {Component} from 'react'
import './listingpackage.css';
import Package from './Package';
import { loader } from "../../../Redux/Actions";
import PlanService from '../../../Services/PlansService';
import { connect } from "react-redux";
class ListingPackges extends Component{
    
    constructor(props){
        super(props)
        this.state={
            plans:null
        }
    }
    componentWillMount(){
        this.props.loader({show:true});
        PlanService.getAllPlans().then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        plans: response.data.data
                    }) 
                    console.log(this.state.plans)
                }
            },
            (error) => {
                this.props.loader({show:false});
                console.log(error)
                throw error;
            }
        )
    }
    render(){
        return(<div>
            {this.state.plans!==null?
            <section id="pricing_table" className="section-padding parallex-bg">
	<div className="container">
    	<div className="section-header white-text div_zindex text-center">
        	<h2>Listing Packages</h2>
            <p>Listing Package prices</p>
        </div>
        
        <div className="row">
        {this.state.plans.map((rowData) => (
            
                <Package data={rowData}/>
                ))}
        </div>
    </div>
	
</section>:null}
</div>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ListingPackges);