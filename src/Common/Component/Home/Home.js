import React, {Component} from 'react';
import SearchBox from '../SearchBox/SearchBox';
import SimpleSlider from '../CategorySlider/SimpleSlider';
import PopularListing from '../PopularListing/PopularListing';
import WhyTownHub from '../WhyTownHub/WhyTownHub';
import ListingPackges from '../ListingPackages/ListingPackages';
import AboutUs from '../AboutUs/AboutUs';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

class Home extends Component{
    render(){
        
        return(
            <div>
                <Header/>
                <SearchBox/>
                <SimpleSlider/>
                <PopularListing/>
                <WhyTownHub/>
                <ListingPackges/>
                <Footer/>
            </div>
        );
    }
}
export default Home;