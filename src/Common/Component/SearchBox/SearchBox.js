import React,{Component} from 'react';
import './search.css';
import categoryService from '../../../Services/categoryService'
import ListingService from '../../../Services/ListingService';
class SearchBox extends Component{
    constructor(props){
        super(props)
        this.state={
            allCategories:null,
            allListing:null
        }
    }
    componentWillMount(){
            categoryService.getAllCategories().then(
                (response) => {
                    if (response.status === 200) {
                        this.setState({
                            allCategories: response.data.data
                        }) 
                        ListingService.getAllListing().then(
                            (response) => {
                                
                                if (response.status === 200) {
                                    this.setState({
                                        allListing: response.data.data
                                    }) 
                                    console.log(this.state.allListing)
                                }
                            },
                            (error) => {
                                
                                console.log(error)
                                throw error;
                            }
                        )
                        console.log(this.state.allCategories)
                    }
                },
                (error) => {
                    console.log(error)
                    throw error;
                }
            )
        }
    render(){
        
        return(
            <div>
                
                <section id="banner" className="parallex-bg section-padding">
	<div className="container">
    	<div className="intro_text white-text div_zindex">
            {window.location.pathname==="/"?<div><h1>Welcome to TownHub</h1>
	        <h5>Search and apply to millions of Listings</h5></div>:null}
    		
            <div className="search_form">
            	<form>
                	<div className="form-group select">
                   		<select className="form-control">
                	    	<option>What are you looking for?</option>
                            {this.state.allCategories?.map((rowData) => (
                                <option>{rowData.categoryName}</option>
                            ))}
	                    </select>
                    </div>
                    <div className="form-group">
                   		<input type="text" className="form-control" placeholder="Search By Listings"/>
                    </div>
                    <div className="form-group search_btn">
                    	<input type="submit" value="Search" className="btn btn-block"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div className="dark-overlay"></div>
</section>

            </div>
        );

    }
}
export default SearchBox