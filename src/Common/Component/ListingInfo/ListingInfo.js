import React, {Component} from 'react';
import Listing2 from '../ListingGrid/Listing2';
import Header from '../Header/Header';
import { loader } from "../../../Redux/Actions";
import { connect } from "react-redux";
import Footer from '../Footer/Footer';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import UtilsService from '../../../Services/UtilsService'
import './ListingInfo.css';
import { Redirect } from 'react-router-dom'
import MessageModel from './MessageModel';
import ListingService from '../../../Services/ListingService';

class ListingInfo extends Component{
    constructor(props){
        super(props)
        this.state={
            isMessageOpen:false,
            allPicture:null,
            listProps:this.props.location?.listinfoProps?.data,
            contactInfo:null,
            addressInfo:null,
            listingTimeInfo:null,
            allListing:null,
        }
        
        this.handleMessageBox=this.handleMessageBox.bind(this);
    }
    handleMessageBox(){
        this.setState({
            isMessageOpen:!this.state.isMessageOpen
        })
    }
    componentWillReceiveProps(){
        if(this.state.listProps!==this.props.location?.listinfoProps?.data){
            this.setState({
                listProps:this.props.location?.listinfoProps?.data
            })
            this.componentWillMount()
        }
        
    }
    componentWillMount(){
        this.props.loader({show:true});
        UtilsService.getAddressById(this.state.listProps?.addressId).then(
            (response) => {
                if (response.status === 200) {
                    this.setState({
                        addressInfo:response.data.data})
                }
                    
                  UtilsService.getContactById(this.state.listProps?.contactId).then(
                    (response) => {
                        if (response.status === 200) {
                            this.setState({
                                contactInfo:response.data.data
                            })
                            console.log(this.state.contactInfo)
                            ListingService.getListingTime(this.state.listProps?.listingId).then(
                                (response) => {
                                    if (response.status === 200) {
                                        this.setState({
                                            listingTimeInfo:response.data.data[0]
                                        })
                                        console.log(this.state.listingTimeInfo)
                                        this.props.loader({show:false});
                                    }
                                  },
                                  (error) => {
                                    console.log(error);
                                    this.props.loader({show:false});
                                   
                                  }
                                );
                        }
                      },
                      (error) => {
                        console.log(error);
                        this.props.loader({show:false});
                       
                      }
                    );
                
              },
              (error) => {
                console.log(error);
                this.props.loader({show:false});
              }
            );
            
            ListingService.getAllListing().then(
                (response) => {
                    
                    if (response.status === 200) {
                        this.setState({
                            allListing: response.data.data
                        }) 
                        console.log(this.state.allListing)
                    }
                },
                (error) => {
                    
                    console.log(error)
                    throw error;
                }
            )
    }
    renderRedirect = () => {
        
        if (!this.props.location.listinfoProps) {
          return <Redirect to='/listinggrid' />
        }
      }
    render(){
        
        const settings = {
			dots: true,
			infinite: true,
			slidesToShow:4,
			slidesToScroll: 1,
			autoplay: true,
			speed: 1000,
			autoplaySpeed: 5000,
            cssEase: "linear"
          };
          const settings2 = {
			dots: true,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			speed: 1000,
			autoplaySpeed: 5000,
			cssEase: "linear"
		  };
        return(
            <div>
                {this.renderRedirect()}
                <Header/>
                <section id="inner_pages">
                
                <section id="listing_detail_banner" className="parallex-bg">
	<div id="listing_img_slider">
    <Slider {...settings}>
    <div className="item"><img src={ require('./gallery_1.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_2.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_3.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_4.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_3.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_4.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_3.jpg') } alt="image"/></div>
           <div className="item"><img src={ require('./gallery_4.jpg') } alt="image"/></div>
          
           <div className="item"><img src={ require('./gallery_5.jpg') } alt="image"/></div>
			</Slider>
		
           
           
    </div>
   
</section>
<section className="listing_detail_header">
	<div className="container">
    	<h1>{this.state.listProps?.listingName} &nbsp; {this.state.listProps?.listingVerify?<span style={{fontSize:"20px"}} className="review_score"><i className="fa fa-check" aria-hidden="true"></i> Verified</span>:null} </h1>
        <p>{this.state.listProps?.listingTagLine}</p>
        <div className="listing_rating">
            <p><span className="review_score">4.2/5</span> 
               <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> 
               <i className="fa fa-star active"></i> <i className="fa fa-star"></i> 
               (5 Reviews) </p>
            <p className="listing_like"><a href="#"><i className="fa fa-heart-o"></i> 5 Likes</a></p>
            <p className="listing_favorites"><a href="#"><i className="fa fa-bookmark-o"></i> Add to favorites</a></p>   
        </div>   
    </div>
</section>
<section className="listing_info_wrap">
	<div className="container">
    	<div className="row">
        	<div className="col-md-8">
            	<div className="ElemoListing_detail">
                	<div className="pricing_info">
                    	<p className="listing_price"><span>₹</span>{this.state.listProps?.minPrice}-<span>₹</span>{this.state.listProps?.maxPrice}</p>
                        <div className="listing_message"><button onClick={this.handleMessageBox} className="btn btn-block"><i className="fa fa-envelope-o"></i> Send Message</button></div>
                    </div>
                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingOne">
                              <h4 className="panel-title">
                                  <i className="fa  fa-file-text-o"></i> Listing Description
                                 
                                
                              </h4>
                            </div>
                            <div id="description" className="panel-collapse" role="tabpanel" aria-labelledby="headingOne">
                              <div className="panel-body">
                                <p>{this.state.listProps?.listingDescription}</p>
                              </div>
                            </div>
                          </div>
                          
                        
                          
                          <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingThree">
                              <h4 className="panel-title">
                                <i className="fa fa-calendar-check-o"></i> Opening Hours
                              </h4>
                            </div>
                            <div id="opening_hours" className="panel-collapse" role="tabpanel" >
                              <div className="panel-body">
                                <ul>
                                	<li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Monday</span> 
                                        <span>{this.state.listingTimeInfo?.mondayOpen!==""?this.state.listingTimeInfo?.mondayOpen:"Closed"}</span> {this.state.listingTimeInfo?.mondayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.mondayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Tuesday</span> 
                                        <span>{this.state.listingTimeInfo?.tuesdayOpen!==""?this.state.listingTimeInfo?.tuesdayOpen:"Closed"}</span> {this.state.listingTimeInfo?.tuesdayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.tuesdayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Wednesday</span> 
                                        <span>{this.state.listingTimeInfo?.wednesdayOpen!==""?this.state.listingTimeInfo?.wednesdayOpen:"Closed"}</span> {this.state.listingTimeInfo?.wednesdayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.wednesdayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Thursday</span> 
                                        <span>{this.state.listingTimeInfo?.thursdayOpen!==""?this.state.listingTimeInfo?.thursdayOpen:"Closed"}</span> {this.state.listingTimeInfo?.thursdayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.thursdayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Friday</span> 
                                        <span>{this.state.listingTimeInfo?.fridayOpen!==""?this.state.listingTimeInfo?.fridayOpen:"Closed"}</span> {this.state.listingTimeInfo?.fridayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.fridayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Saturday</span> 
                                        <span>{this.state.listingTimeInfo?.saturdayOpen!==""?this.state.listingTimeInfo?.saturdayOpen:"Closed"}</span> {this.state.listingTimeInfo?.saturdayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.saturdayClose}</span>
                                    </li>
                                    <li>
                                    	<span className="hours_title"><i className="fa fa-clock-o"></i>Sunday</span> 
                                        <span>{this.state.listingTimeInfo?.sundayOpen!==""?this.state.listingTimeInfo?.sundayOpen:"Closed"}</span> {this.state.listingTimeInfo?.sundayOpen!==""?"-":""} <span>{this.state.listingTimeInfo?.sundayClose}</span>
                                    </li>
                                </ul>
                              </div>
                            </div>
                          </div>
						   
                    </div>
                        <div className="reviews_list">
                            <div className="widget_title">
                                 <h4><span>1 Review for</span> {this.state.listProps?.listingName}</h4>
                             </div>
                            
                            <div className="review_wrap">
                                <div className="review_author">
                                    <img src={ require('./happy-client-01.jpg') } alt="image"/>
                                    <figcaption>
                                        <h6>Priyanka Shah</h6>
                                    </figcaption>
                                </div>
                                <div className="review_detail">
                                    <h5>Good service and Location is great</h5>
                                    <p>Review description</p>
                                    <div className="listing_rating">
                                        <p><span className="review_score">4.0/5</span> 
                                           <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> 
                                           <i className="fa fa-star active"></i> <i className="fa fa-star"></i> 
                                           (5 Reviews) </p>
                                        <p><i className="fa fa-clock-o"></i> April 11, 2017 8:52 am</p>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div id="writereview" className="review_form">
                    	<div className="widget_title">
	                    	 <h4>Write a Review </h4>
                         </div>
						<form action="#" method="get">
                        	<div className="form-group">
                            	<label className="form-label">Your Rating for this listing</label>
                                <div className="listing_rating">
                                    <input name="rating" id="rating-1" value="1" type="radio"/>
                                    <label for="rating-1" className="fa fa-star"></label>
                                    <input name="rating" id="rating-2" value="2" type="radio"/>
                                    <label for="rating-2" className="fa fa-star"></label>
                                    <input name="rating" id="rating-3" value="3" type="radio"/>
                                    <label for="rating-3" className="fa fa-star"></label>
                                    <input name="rating" id="rating-4" value="4" type="radio"/>
                                    <label for="rating-4" className="fa fa-star"></label>
                                    <input name="rating" id="rating-5" value="5" type="radio"/>
                                    <label for="rating-5" className="fa fa-star"></label>
                                </div>
                            </div>
                            <div className="form-group">
                            	<label className="form-label">Email</label>
                                <input name="" type="email" placeholder="you@website.com" className="form-control"/>
                            </div>
                            <div className="form-group">
                            	<label className="form-label">Title</label>
                                <input name="" type="text" placeholder="Title of Your Review" className="form-control"/>
                            </div>
                            <div className="form-group">
                            	<label className="form-label">Review</label>
                                <textarea name="" cols="" rows="" className="form-control" placeholder="Yout Experience"></textarea>
                            </div>
                            <div className="form-group">
                            	<input type="submit" className="btn" value="Submit Review"/>
                            </div>
                        </form>
                    </div>
            	</div>
            </div>
            

			 <div className="col-md-4">
            	<div className="ElemoListing_sidebar">
                {	//<div className="sidebar_wrap listing_action_btn">
                    //	<ul>
                            
                            //<li><a data-toggle="modal" data-target="#email_friends_modal"><i className="fa fa-envelope-o"></i>Email to Business</a></li>
                            //<li><a href="#writereview" className="js-target-scroll"> <i className="fa fa-star"></i> Write a Review</a></li>
                            
                   //     </ul>
                 //   </div>
                }
                <div className="sidebar_wrap listing_contact_info">
                <div className="widget_title">
	                    	<h6>Address Info</h6>
                        </div>
                    	<ul>
                            <li><i className="fa fa-building-o"></i> {this.state.addressInfo?.addressLine1},{this.state.addressInfo?.addressLine2}</li>
                            <li><i className="fa fa-map-marker"></i> {this.state.addressInfo?.area}</li>
                            <li><i className="fa fa-address-book"></i> {this.state.addressInfo?.city} , {this.state.addressInfo?.state} , {this.state.addressInfo?.country}</li>
                            
                        </ul>
                  </div>
                	<div className="sidebar_wrap listing_contact_info">
                    	<div className="widget_title">
	                    	<h6>Contact Info</h6>
                        </div>
                    	<ul>
                        	
                            <li><i className="fa fa-phone"></i> <a href={"tel:"+this.state.contactInfo?.phoneNumber}>{this.state.contactInfo?.phoneNumber}</a></li>
                            <li><i className="fa fa-envelope"></i><a href={"mailto:"+this.state.listProps?.email}> {this.state.listProps?.email}</a></li>
                            <li><i className="fa fa-link"></i> <a href={this.state.listProps?.website}> {this.state.listProps?.website}</a></li>
                            {this.state.contactInfo?.workNumber===""?null:<li><i className="fa fa-phone"></i> <a href={"tel:"+this.state.contactInfo?.workNumber}>{this.state.contactInfo?.workNumber}</a></li>}
                        </ul>
                        
                    </div>
                    
                    
                </div>
            </div>
            
        </div>
    </div>
</section>

{this.state.allListing?.filter(data=>data.categoryId===this.state.listProps?.categoryId||data.city===this.state.listProps?.city)?
    <section id="similar_listings" className="section-padding gray_bg">
	<div className="container">
		<div className="section-header text-center">
            <h2>Similar Listings </h2>
        </div>
        
            <Slider {...settings2}>
        {this.state.allListing?.filter(data=>data.categoryId===this.state.listProps?.categoryId||data.city===this.state.listProps?.city).map((rowData) => {
            if(rowData.listingId!==this.state.listProps?.listingId){
               return( <div className="sliderlist">
            <Listing2 data={rowData}/>
            </div>)
            }
            
        })}
            </Slider>
        
    </div>
</section>:null
}

</section>
{this.state.isMessageOpen?<MessageModel action={this.handleMessageBox} listingId={this.state.listProps?.listingId}/>:null}

<Footer/>
            </div>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}
export default connect(null, mapDispatchToProps)(ListingInfo);