import React, {Component} from 'react';
import logo from './logo.png'
import Modal from 'react-modal';
import './header.css';
import {Link} from 'react-router-dom';
import Login from '../Login/Login';
import swal from 'sweetalert';
import {Redirect} from 'react-router-dom'
class Header extends Component{
  constructor(props){
    super(props);
    this.state={
      isLoginShow:false,
      isLoggedin:false,
      isLoggedinUser:null
    }
    
    this.handleLogin=this.handleLogin.bind(this)
    this.logout=this.logout.bind(this)
    this.handleListing=this.handleListing.bind(this)
  }
  returnMenu= () =>{
    switch(this.state.isLoggedinUser.role){
        case "admin":
            return(
                <ul className="sub-menu">
                <li><Link to="/dashboard" > Dashboard</Link></li>
                <li><Link to="/dashboard/myprofile"> My Profile</Link></li>
                <li><Link to="/dashboard/changepassword"> Change Password</Link></li>
                <li><Link to="/dashboard/managecategory"> Manage Category</Link></li>
                <li><Link to="/dashboard/manageplans"> Manage Plans</Link></li>
                <li><Link to="/dashboard/managequeries"> Manage Queries</Link></li>
                <li><Link to="/dashboard/manageusers/user">Manage Users</Link></li>
                <li><Link to="/dashboard/managelisting"> Manage Listings</Link></li>
                <li><Link to="/dashboard/manageusers/business"> Manage Business</Link></li>
                <li><Link to="/dashboard/managebusinessplan"> Manage Business Plans</Link></li>
                <li><Link onClick={this.logout}> Logout</Link></li>
            </ul>	
            )
            case "business":
            return(
                <ul className="sub-menu">
                <li><Link to="/dashboard" > Dashboard</Link></li>
                <li><Link to="/dashboard/addnewlist">Add New Listings</Link></li>
                <li><Link to="/dashboard/mylisting" data-toggle=""> My Listings</Link></li>
                <li><Link to="/dashboard/reviews">Reviews</Link></li>
                <li><Link to="/dashboard/myprofile"> My Profile</Link></li>
                <li><Link to="/dashboard/changepassword"> Change Password</Link></li>
                <li><Link to="/dashboard/managequeryBusiness">Manage Queries</Link></li>
                <li><Link onClick={this.logout}> Logout</Link></li>
            </ul>	
            )
            case "user":
            return(
                <ul className="sub-menu">
                <li><Link to="/dashboard" >Dashboard</Link></li>
                <li><Link to="/dashboard/mylisting" >My Listings</Link></li>
                <li><Link to="/dashboard/myprofile"> My Profile</Link></li>
                <li><Link to="/dashboard/changepassword">Change Password</Link></li>
                <li><Link onClick={this.logout}> Logout</Link></li>
            </ul>	
            )
            default:
                break;
    }
     
   
}
  handleListing =()=>{
      if(this.state.isLoggedin){
          swal("OOPS..","Upgrade yourself to business account so that you can add new listing.","error")
      }else{
        swal("OOPS..","You Must Be Login First To Add A Listing","error");
        this.handleLogin()
      }
  }
  checkLogin(){
    if(sessionStorage.getItem("loginState")){
      this.setState({
        isLoggedin:true,
        isLoggedinUser:JSON.parse(sessionStorage.getItem("loginState"))
      })
    }
  }
  componentWillMount(){
    if(sessionStorage.getItem("loginState")){
      this.setState({
        isLoggedin:true,
        isLoggedinUser:JSON.parse(sessionStorage.getItem("loginState"))
      })
    }
    Modal.setAppElement('body');
  }
  handleLogin(){
    this.setState({
      isLoginShow:!this.state.isLoginShow
    },this.checkLogin)
  }
  logout(){
    this.setState({
      isLoggedin:false,
      isLoggedinUser:null,
      isLoginShow:false
    })
    sessionStorage.clear()
    swal("You're Logged Out.", "Thank You For Using Our Services. We love to see you again soon..", "success");
  }
  render(){
  return (
    <div>
      {!this.state.isLoggedin?<Modal isOpen={this.state.isLoginShow} onRequestClose={this.handleLogin} style={{overlay:{
       background:'rgba(255,255,255,0.8)',
       zIndex:2
      },
      content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        background:'rgba(255,255,255,0.0)',
        paddingTop: '120px',
        border:0,
       
      }}}>
        <Login checkLogin={()=>this.checkLogin()} checkSignup={()=>this.checkSignup()}/>
      </Modal>:null}
      
        <header id="header" className="transparent-header">
    <nav className="navbar navbar-expand-lg fixed-top" id="header_nav">
        <div className="container-fluid">
		<div className="row header_row">	
		<div className="col-md-3 col-sm-12 col-xs-12">
          <div className="navbar-header">
            <div className="logo"><Link to="/"><img src={logo} alt="images"/></Link></div>
			</div>
 <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" className="navbar-toggler" type="button"> 
                <i className="fa fa-bars"></i> 
            </button>
		</div>
           
          
<div className="col-md-9 col-sm-12 col-xs-12">
          <div className="collapse navbar-collapse" id="navigation">
            <ul className="nav navbar-nav mr-auto">
              <li><Link to="/">Home</Link></li>
              <li><Link to="/listinggrid">Listing</Link></li>
              <li><Link to="/contactus">Contact Us</Link></li>
              <li><Link to="/howitwork">How It Works</Link></li>
              <li><Link to="/aboutus">About Us</Link></li>
              {this.state.isLoggedin?<li className="menu-item-has-children"><Link to="/">Hello, {this.state.isLoggedinUser.username}</Link> <span className="arrow"></span>
              	 {this.returnMenu()}
              </li>:<li className="signinLi" onClick={this.handleLogin}>&nbsp;&nbsp;Sign In&nbsp;</li>}
    
   
            </ul>
            <div className="submit_listing">{this.state.isLoggedinUser?.role==="business"?<Link to="/dashboard/addnewlist" className="btn outline-btn"><i className="fa  fa-plus-circle"></i> Submit Listing</Link>:
	            <button onClick={this.handleListing} className="btn outline-btn"><i className="fa  fa-plus-circle"></i> Submit Listing</button>}
            </div>
          </div>
</div>
		</div>
        </div>
    </nav>
</header>
    </div>
  );
  }
}

export default Header;
