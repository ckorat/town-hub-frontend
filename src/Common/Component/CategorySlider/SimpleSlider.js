import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './category.css';
import Category from "./Category";
import categoryService from '../../../Services/categoryService'
import { loader } from "../../../Redux/Actions";
import { connect } from "react-redux";

class SimpleSlider extends React.Component {
  constructor(props){
    super(props)
    this.state={
      allCategories:null
    }
  }
  componentWillMount(){
    this.props.loader({show:true});
        categoryService.getAllCategories().then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        allCategories: response.data.data
                    }) 
                    console.log(this.state.allCategories)
                }
            },
            (error) => {
                this.props.loader({show:false});
                console.log(error)
                throw error;
            }
        )
    }
    render() {
        const settings = {
          dots: true,
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          speed: 1000,
          autoplaySpeed: 5000,
          cssEase: "linear"
        };
        return (
            <section id="listing_category" className="section-padding">
	<div className="container">
    	<div className="section-header text-center">
        	<h3>List of Categories you might be interested in</h3>
            <p>Select any category you are in search of!!!</p>
        </div>
            
        {this.state.allCategories===null?
                <div className="add_listing_info text-center">
                    <h4>Sorry We Couldn't able to find any category for you right now.</h4>
                </div>:
            <Slider {...settings}>
                {this.state.allCategories.map((rowData) => (
                <Category data={rowData}/>
                ))}
                
            </Slider>}
          </div>
        
          </section>
        );
      }
    }
    const mapDispatchToProps = (dispatch) => {
      return {
          loader:(payload)=>{
              dispatch(loader(payload));
          }
      }
  }
  
  
  export default connect(null, mapDispatchToProps)(SimpleSlider);