import React,{Component} from 'react';
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from './noImage.jpg';
import loader from '../../../Loader/abc.gif'
import categoryService from '../../../Services/categoryService'
class Category extends Component{
    constructor(props){
        super(props)
        this.state={
            categoryCount:null
        }
    }
    componentWillMount(){
        categoryService.getCategoryCount(this.props.data.categoryId).then(
            (response) => {
                if (response.status === 200) {
                    this.setState({
                        categoryCount: response.data.data
                    }) 
                    console.log(this.state.categoryCount)
                }
            },
            (error) => {
                
                console.log(error)
                throw error;
            }
        )
    }
    setErrorImage = (e) => {
        e.target.src = noImage;
    }
    render(){
        let scrollPosition = 0;
        return(
            <div id="category_slider2" >
            <div  className=" item" style={{ backgroundImage: `url(${this.props.data.categoryBigIconURL})`}}>
                	<a href="#">
    	            	<div className="category_icon">
                        	<span className="category_listing_n">{this.state.categoryCount?this.state.categoryCount:"0"}</span>
                            <LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loader} alt={'category_image'} src={this.props.data.categorySmallIconURL} scrollPosition={scrollPosition} effect="" />
                        </div>
        <p>{this.props.data.categoryName}</p>
                    </a>
                </div>
                </div>
        )
    }
}
export default trackWindowScroll(Category)