import React,{Component} from 'react';
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../CategorySlider/noImage.jpg';
import {Link} from 'react-router-dom';

import loader from '../../../Loader/abc.gif'
class Listing2 extends Component{
    constructor(props){
        super(props)
    }
    setErrorImage = (e) => {
        e.target.src = noImage;
    }
    render(){
        let scrollPosition = 0;
        return(
            
                <div className="col-md-4 grid_view show_listing">
            	<div className="listing_wrap">
                    <div className="listing_img">
                                            <span className="like_post"><i className="fa fa-bookmark-o"></i></span>
                       <div className="listing_cate">
                            <span className="cate_icon"><Link to={{pathname:"/listinggrid",listinfoProps:{data:this.props.data}}}><img src={this.props.data.categorySmallIcon} alt="icon-img"/></Link></span> 
                            <span className="listing_like"><a href="#"><i className="fa fa-heart-o"></i></a></span>
                       </div>
<Link to={{pathname:"/listinginfo",listinfoProps:{data:this.props.data}}} ><LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loader} alt={'category_image'} src={this.props.data.listingImage} scrollPosition={scrollPosition} effect="" /></Link>
                    </div>
                    <div className="listing_info">
                        <div className="post_category">
                        <Link to={{pathname:"/listinggrid",listinfoProps:{data:this.props.data.categoryId}}}>{this.props.data.categoryName}</Link>
                        </div>
                            <h4><Link to={{pathname:"/listinginfo/",listinfoProps:{data:this.props.data}}} >{this.props.data.listingName}</Link></h4>
                        <p>{this.props.data.listingTagLine}</p>
                        
                        <div className="listing_review_info">
                        <p><span className="review_score">4.0/5</span> 
                               <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> <i className="fa fa-star active"></i> 
                               <i className="fa fa-star active"></i> <i className="fa fa-star"></i> 
                               (5 Reviews) </p>
                               
                            <p className="listing_map_m"><i className="fa fa-map-marker"></i> {this.props.data.city}</p>
                        </div>
                    </div>
                </div>
            </div>
            
        )
    }
}
export default trackWindowScroll(Listing2)