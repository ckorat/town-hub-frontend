import React,{Component} from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import './ListingGrid.css'
import Listing2 from './Listing2';
import { loader } from "../../../Redux/Actions";
import SearchBox from '../SearchBox/SearchBox';
import { connect } from "react-redux";
import ListingService from '../../../Services/ListingService';
class ListingGrid extends Component{
    constructor(props){
        super(props)
        this.state={
            allListing:null
        }
    }
    componentWillMount(){
        console.log(this.props.location.listinfoProps);
        this.props.loader({show:true});
        ListingService.getAllListing().then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        allListing: response.data.data
                    }) 
                    console.log(this.state.allListing)
                }
            },
            (error) => {
                this.props.loader({show:false});
                console.log(error)
                throw error;
            }
        )
    }
    render(){
        
        return(
            <div>
                <Header/>
                <SearchBox/>
                <section id="inner_pages">
	<div className="container">
    	<div className="listing_header">
        	<h5>All Listings</h5>
            
            <div className="layout-switcher">
               
            </div>
        </div>
    	
    	<div className="row">
        	
        {this.state.allListing!==null?this.state.allListing.map((rowData) => (
            
            <Listing2 data={rowData}/>
            )):<div className="add_listing_info">
            <h3>Sorry We Coundn't able to find any listing for you right now. Please Try Again Later.</h3>
        </div>}
        </div>
		
                { // <ul className="pagination">
                   // <li className="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    //<li className="active"><a href="#">1</a></li>
                    //<li><a href="#">2 </a></li>
                   // <li><a href="#">3 </a></li>
                    //<li><a href="#">4 </a></li>
                    //<li><a href="#">5 </a></li>

                    //<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                  //</ul>
                }
    </div>
</section>
<Footer/>
            </div>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ListingGrid);