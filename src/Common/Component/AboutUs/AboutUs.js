import React,{Component} from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import './aboutUs.css';
class AboutUs extends Component{
render()
   {
            return(
                <div>
                    <Header/>
                    <section id="aboutus_bg" className="parallex-bg aboutus_bg">
                        
	<div className="container">
    	<div className="white-text text-center div_zindex">
        	<h1>About Us </h1>
        </div>
    </div>
    <div class="dark-overlay"></div>
</section>
                    
                    <section id="inner_pages">
	                    <div className="container">
    	                    <div className="row">
                        <div className="col-md-6">
            	                <h3>Why We Are Unique</h3>
                            <p>The main objective of the TownHub web application is to provide the best option to the users about the nearby service, it gives you a feature to find the service as per your satisfaction. </p>
                            <p>This will also provide a benefit to the business, who all are registered with this application by promoting their business and allowing them to connect to the users easily. </p>
                            <p>The TownHub web application will help you to make your life easier by providing all the service information which you need in no time, you can find most of the popular service providers, new places to hang out in one place.</p>
                             </div>
                         <div className="col-md-6">
                                  <div className="about_wrap">
            	            <img src={ require('./aboutUs.png') } alt="image"/>
                            </div>
                        </div>	
                            </div>	
        
        <div className="row">
        	<div className="col-md-4">
            	<div className="box_wrap">
                	<i className="fa fa-file-text-o" aria-hidden="true"></i>
					<h4>Listings</h4>
                    <p>We have a chunk of listings for you to pick from as per your category</p>
                </div>
            </div>
            
            <div className="col-md-4">
            	<div className="box_wrap">
                	<i className="fa fa-money" aria-hidden="true"></i>
					<h4>Save Time & Money </h4>
                    <p>We can save time & money by selecting list wisely from this website</p>
                </div>
            </div>
            
            <div className="col-md-4">
            	<div className="box_wrap">
                	<i className="fa fa-thumbs-o-up" aria-hidden="true"></i>
					<h4>Best Services</h4>
                    <p>We make sure we provide your best service to the users as well as to the business</p>
                </div>
            </div>
        </div>
        
        <div className="fan_facts gray_bg">
        	<div className="section-header text-center">
    	    	<h3>We Are Here To Help You To Grow Your Business</h3>
	            <p>Here service providers are supposed to provide all the information like contact details, addresses, and services. You can easily make a decision as per the best place in the whole town from a location or through the best ratings and reviews given by end-users.</p>
                <p>This website will mainly provide facilities to the user to find and make decisions about selecting the proper service which is best for the user. Basically, for users, it becomes easy by seeing the reviews, ratings, information provided by the service providers. So that they can make decisions that fit perfectly for them.</p>
            </div>
        </div>
    </div>
</section>
<Footer/>
                </div>
            );
        }
}
export default AboutUs;