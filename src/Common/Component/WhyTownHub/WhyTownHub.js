import React,{Component} from 'react'
import './whytownhub.css';
class WhyTownHub extends Component{
    render(){
        return(
            <section id="why-choose" className="section-padding">
	<div className="container">
		<div className="section-header text-center">
			<h2>Why TownHub for your Business?</h2>
			<p>TownHub provides the various ways that help you can
					earn money from your site.</p>
		</div>
		<div className="row">
			<div className="col-md-6">
				<div className="about-img text-center">
					<img src={ require('./about-img-3.png') } alt="image"></img>
				</div>
			</div>
			<div className="col-md-6">
				<div className="why-list">
					<div className="list-icon-wp">
						<i className="fa fa-check"></i>
					</div>
					<h5>Claim Listing</h5>
					<p>Another revenue model to monetize from listing. You create a listing on your site and allow the business owner to claim it.</p>
				</div>
				<div className="why-list">
					<div className="list-icon-wp">
						<i className="fa fa-money"></i>
					</div>
					<h5>Paid Listing</h5>
					<p>This is the big money maker of your directory site. Listing owners will pay to get their places listed on your site.</p>
				</div>
				<div className="why-list">
					<div className="list-icon-wp">
						<i className="fa fa-area-chart"></i>
					</div>
					<h5>Promote your Business</h5>
					<p>You offer Promotion Plans, listing owners will have directories appeared at special spots on site, and at the top of search results page.</p>
				</div>
				<a href="" className="btn mr-2">See Our Pricing</a>
				<a href="" className="btn black-bg">Submit Listing</a>
			</div>
		</div>
	</div>
</section>
        );

    }
}
export default WhyTownHub