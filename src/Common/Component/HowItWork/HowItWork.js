import React, { Component } from "react";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import "./howItWork.css";
import {Link} from 'react-router-dom';
class HowItWork extends Component {
  render() {
    return (
      <div>
          <Header/>
        <section id="how_it_work_bg" className="parallex-bg">
          <div className="container">
            <div className="white-text text-center div_zindex">
              <h1>How it Works </h1>
            </div>
          </div>
          <div className="dark-overlay"></div>
        </section>
        <section id="inner_pages">
	<div className="container">
		<div className="row">
        	<div className="col-sm-6">
            	<div className="how_it_work_info">
                	<span className="step_n">01</span>
                	<h3>Register an Account</h3>
		            <p>If you are a business or a user you need to create an account on the website. 
                  In which basic details needs to be provided to Sign Up. 
                  And after Sign Up business will be able to go on the step 2. 
                  And business can choose the plans as per its requirement. </p>
                </div>
            </div>
            <div className="col-sm-6">
            	<div className="how_it_work_img">
		            <img src={ require('./step-1.PNG') } alt="image"/>
                </div>
            </div>
        </div>
        
        <div className="row">
        	<div className="col-sm-6">
            	<div className="how_it_work_info">
                	<span className="step_n">02</span>
                	<h3>Choose a Plan </h3>
		            <p>Here different types of Plans options will be there from which Business owners need to 
                  select any one Plan as per its requirement.
                  Choose Plans the way you want your listing to be displayed.</p>
                </div>
            </div>
            <div className="col-sm-6">
            	<div className="how_it_work_img">
		            <img src={ require('./step-2.PNG') } alt="image"/>
                </div>
            </div>
        </div>
        
        <div className="row">
        	<div className="col-sm-6">
            	<div className="how_it_work_info">
                	<span className="step_n">03</span>
                	<h3>Submit Your Listing</h3>
		            <p>By clicking on "+ Submit Listing" Button on the top right corner or going from the Business dashboard after Business login , to Add Listing option
                  from the Menu. Where Business will need to provide the Listing informations. From adding the Listing informations and making it Active so that, it could be visible on the users side on the Website.
                </p>
                </div>
            </div>
            <div className="col-sm-6">
            	<div className="how_it_work_img">
		            <img src={ require('./step-3.PNG') } alt="image"/>
                </div>
            </div>
        </div>    
    </div>
</section>
<Footer/>
      </div>
    );
  }
}
export default HowItWork;
