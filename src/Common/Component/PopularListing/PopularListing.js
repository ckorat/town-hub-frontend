import React, {Component} from 'react';
import Listing from './Listing';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './popularlisting.css';
import { connect } from "react-redux";
import ListingService from '../../../Services/ListingService';
import { loader } from "../../../Redux/Actions";
class PopularListing extends Component{
	constructor(props){
        super(props)
        this.state={
            allListing:null
        }
    }
    componentWillMount(){
        this.props.loader({show:true});
        ListingService.getAllVerifiedListing().then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        allListing: response.data.data
                    }) 
                    console.log(this.state.allListing)
                }
            },
            (error) => {
                this.props.loader({show:false});
                console.log(error)
                throw error;
            }
        )
    }
    render(){
		const settings = {
			dots: true,
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			speed: 1000,
			autoplaySpeed: 5000,
			cssEase: "linear"
		  };
        return(
            <section id="listing" className="section-padding gray_bg">
	<div className="container">
		<div className="section-header text-center">
			<h2>Popular Verified Listings</h2>
			<p>Hotels, Resorts, Motels… It’s there for your choosing. Do a search or check TownHub selection below.</p>
		</div>
		
		
		{this.state.allListing!==null?<div id="category_slider1">
			{this.state.allListing.length<4?
			<div className="row">
				{this.state.allListing.map((rowData) => (
            
            <Listing isSlider={false} data={rowData}/>
            ))}
			</div>:
			<Slider {...settings}>
			{this.state.allListing.map((rowData) => (
            
            <Listing isSlider={true} data={rowData}/>
            ))}
			</Slider>	
		}
		</div>:
		<div id="category_slider1">
		<div className="add_listing_info">
            <h3>Sorry We Couldn't able to find any listing for you right now. Please Try Again Later.</h3>
        </div></div>}
		
		
		
	</div>
</section>
        );
    }
}const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(PopularListing);