import React, {Component} from 'react'
import './listing.css';
import {Link} from 'react-router-dom'
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../CategorySlider/noImage.jpg';
import loader from '../../../Loader/abc.gif'

class Listing extends Component{
	constructor(props){
		super(props)
	}
	setErrorImage = (e) => {
        e.target.src = noImage;
    }
    render(){
        let scrollPosition = 0;
        return(
            <div className={this.props.isSlider?"col-md-12":"col-md-4"}>
				<div className="listing-wp">
					<div className="listing-thumb">
					<span className="like_post"><i className="fa fa-bookmark-o"></i></span>
                        <div className="listing_cate">
                            <span className="cate_icon"><img src={this.props.data.categorySmallIcon} alt="icon-img"/></span> 
                            <span className="listing_like"><a href="#"><i className="fa fa-heart-o"></i></a></span>
                        </div>
						<div className="featured_label"> <i className="fa fa-check" aria-hidden="true"></i> Verified</div>
						<Link to={{pathname:"/listinginfo",listinfoProps:{data:this.props.data}}}><LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loader} alt={'category_image'} src={this.props.data.listingImage} scrollPosition={scrollPosition} effect="" /></Link>
					</div>
					<div className="listing-info-wp">
						<h5><Link to={{pathname:"/listinginfo",listinfoProps:{data:this.props.data}}}>{this.props.data.listingName}</Link></h5>
						<div className="listing-rating">
							<i className="fa fa-star active"></i>
							<i className="fa fa-star active"></i>
							<i className="fa fa-star active"></i>
							<i className="fa fa-star active"></i>
							<i className="fa fa-star"></i>
							<span>(15 reviews)</span>
						</div>
						<p>{this.props.data.listingTagLine}</p>
						<div className="listing-categories">
							<a href="#">{this.props.data.categoryName}</a>
						</div>
					</div>
					<div className="listing-footer">
						<i className="fa fa-map-marker"></i> {this.props.data.city}
					</div>
				</div>
			</div>
        );

    }
}
export default Listing