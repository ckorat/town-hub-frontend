import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import SearchBox from '../SearchBox/SearchBox';
import './errorPage.css';
import DashBoardHeader from '../Dashboard/Header/DashBoardHeader';
import Navbar from '../Dashboard/Navbar/Navbar';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
class ErrorPage extends Component{
    
    render(){
        let isDashboard=window.location.href.includes("dashboard")
        return(
            
            <div>
                    {isDashboard?<div><DashBoardHeader/>
                        <br></br>
                
                        <br></br>

                    <Navbar/></div>

                    :<div>
                    <Header/>
                    <SearchBox/>
                    </div>
                    }

                    
                    

                

<section id="inner_pages">
	<div className="container">
    	<div className="not_found_msg text-center">
          <div className="error_msg_div"> 
            <h2><span>404</span>Oops! That page not found</h2>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.</p> 
            <Link to={isDashboard?"/dashboard":"/"} className="btn"> Back To Home </Link>
          </div>
        </div>
    </div>
</section>
{!isDashboard?<Footer/>:null}
            </div>
        );
    }
}
export default ErrorPage;