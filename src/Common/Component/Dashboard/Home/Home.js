import React,{Component} from 'react';
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
class DashBoardHome extends Component{
    render(){
        return(
            <div>
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
		<div id="titlebar">
			<div className="row">
				<div className="col-md-12">
					<h2>Admin/Business DashBoard</h2>
					<nav id="breadcrumbs">
						<ul>
							<li><a href="/">Home</a></li>
							<li>Dashboard</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
        <div className="row">
            <div className="dashboard-info-box">
                <div className="dashboard-info color-1">
                    <h4>6</h4> <span>Published Listings</span>
                </div>
                <div className="dashboard-info color-2">
                    <h4>1</h4> <span>Pending  Listings</span>
                </div>
               
                <div className="dashboard-info color-4">
                    <h4>520</h4> <span>Total Views</span>
                </div>
                <div className="dashboard-info color-5">
                    <h4>820</h4> <span>Total Reviews</span>
                </div>
                <div className="dashboard-info color-6">
                    <h4>15</h4> <span>Bookmarked Listings</span>
                </div>
            </div>
        </div>
       
        </div>
        </div>
                <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
			
            </div>
        </div>
        );
    }
}
export default DashBoardHome