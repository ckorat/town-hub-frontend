import React,{Component} from 'react'
class MessageModel extends Component{
  constructor(props){
    super(props)
  }
    render(){
        return(
            <div id="message_modal" className="modal fade show"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" onClick={this.props.action} className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3 className="modal-title">Send Message</h3>
      </div>
      <div className="modal-body">
        <form>
        <div className="form-group">
          	<input type="text" className="form-control" placeholder="To Email"/>
          </div>
          <div className="form-group">
          	<input type="text" className="form-control" placeholder="Subject"/>
          </div>
          <div className="form-group">
          	<input type="email" className="form-control" placeholder="Your Email"/>
          </div>
          <div className="form-group">
            <textarea rows="4" className="form-control" placeholder="Message"></textarea>
          </div>
          <div className="form-group">
            <input value="Send Message" className="btn btn-block" type="submit"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
        )
    }
}
export default MessageModel