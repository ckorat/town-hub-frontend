import React, { Component } from "react";
import categoryService from "../../../../Services/categoryService";
import SimpleReactValidator from "simple-react-validator";
import { storage } from "../../../../Firebase/index";
import DashBoardHeader from "../Header/DashBoardHeader";
import Navbar from "../Navbar/Navbar";
import "../dashboard.css";
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from "sweetalert";
import { Redirect } from "react-router-dom";
class ManageCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCategories: null,
      categoryName: "",
      categoryDescription: "",
      categorySmallIconURL: null,
      categoryBigIconURL: null,
      smallUrl: "",
      bigUrl: "",
      progress: 0,
      edit: false,
    };
    this.validator = new SimpleReactValidator({ autoForceUpdate: this });
  }
  formReset = () => {
    this.fileInput.value = "";
    this.fileInput2.value = "";
    this.setState({
      categoryName: "",
      categoryDescription: "",
    });
    this.componentWillMount();
  };
  submitForm = (event) => {
    event.preventDefault();
    this.props.loader({ show: true });
    if (this.validator.allValid()) {
      const uploadTask = storage
        .ref(`categories/${this.state.categorySmallIconURL.name}`)
        .put(this.state.categorySmallIconURL);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          this.setState({
            progress: Math.round(
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            ),
          });
        },
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref("categories")
            .child(this.state.categorySmallIconURL.name)
            .getDownloadURL()
            .then((downloadurl) => {
              this.setState({
                smallUrl: downloadurl,
              });
              const uploadTask = storage
                .ref(`categories/${this.state.categoryBigIconURL.name}`)
                .put(this.state.categoryBigIconURL);
              uploadTask.on(
                "state_changed",
                (snapshot) => {
                  this.setState({
                    progress: Math.round(
                      (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    ),
                  });
                },
                (error) => {
                  this.props.loader({ show: false });

                  console.log(error);
                },
                () => {
                  storage
                    .ref("categories")
                    .child(this.state.categoryBigIconURL.name)
                    .getDownloadURL()
                    .then((downloadurl) => {
                      this.setState({
                        bigUrl: downloadurl,
                      });

                      const data = {
                        categoryName: this.state.categoryName,
                        categoryDescription: this.state.categoryDescription,
                        categorySmallIconURL: this.state.smallUrl,
                        categoryBigIconURL: this.state.bigUrl,
                        categoryActive: true,
                      };
                      categoryService.addCategory(data).then(
                        (response) => {
                          if (response.status === 201) {
                            swal(
                              "Success..",
                              "Category Added Successfully",
                              "success"
                            );
                            console.log(response);
                            this.formReset();
                            this.props.loader({ show: false });
                          }
                        },
                        (error) => {
                          this.props.loader({ show: false });
                          swal(
                            "Oops.",
                            "Something Went Wrong. Please Try Again Later.",
                            "error"
                          );
                          console.log(error);
                        }
                      );
                    });
                }
              );
            });
        }
      );
    } else {
      this.props.loader({ show: false });
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  onEdit = (event) => {
    let id = event.target.id;
    event.preventDefault();
    let fetched_data = this.state.allCategories.filter(
      (data) => data.categoryId === +id
    );
    if (fetched_data) {
      this.setState(
        {
          edit: true,
          categoryId: id,
          categoryName: fetched_data[0].categoryName,
          categoryDescription: fetched_data[0].categoryDescription,
          categorySmallIconURL: fetched_data[0].categorySmallIconURL,
          categoryBigIconURL: fetched_data[0].categoryBigIconURL,
          categoryActive: fetched_data[0].categoryActive,
        },
        () => {
          console.log("data:", fetched_data);
        }
      );
    }
  };

  updateForm = (event) => {
    event.preventDefault();
    this.props.loader({ show: true });
    // if (this.validator.allValid()) {
      if(this.state.categoryId && this.state.categoryName && this.state.categoryDescription){
      const data = {
        categoryId: this.state.categoryId,
        categoryName: this.state.categoryName,
        categoryDescription: this.state.categoryDescription,
        categorySmallIconURL: this.state.categorySmallIconURL,
        categoryBigIconURL: this.state.categoryBigIconURL,
        categoryActive: this.state.categoryActive,
      }
      swal({
        title: "Are you sure?",
        text: "You Really Want To Edit This Category",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willUpdate) => {
        if (willUpdate) {
          categoryService.updateCategory(data).then(
            (response) => {
              if (response.status === 200) {
                swal("Success..", "Category Updated Successfully", "success");
                console.log(response);
                // this.formReset()
                this.props.history.push("/");
                this.props.history.push("/dashboard/managecategory");
                this.props.loader({ show: false });
              }
            },
            (error) => {
              this.props.loader({ show: false });
              swal(
                "Oops.",
                "Something Went Wrong. Please Try Again Later.",
                "error"
              );
              console.log(error);
            }
          );
        } else {
          this.props.loader({ show: false });
          this.validator.showMessages();
          this.forceUpdate();
        }
      });
    } else {
      this.props.loader({ show: false });
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  handleChange = (event) => {
    let change = {};
    change[event.target.name] = event.target.value;
    this.setState(change);
  };
  handleFileChange = (event) => {
    if (event.target.files[0]) {
      if (event.target.name === "categorySmallIconURL") {
        this.setState({
          categorySmallIconURL: event.target.files[0],
        });
        const categoryImage = event.target.files[0];
        this.setState({
          categorySmallIconURL: categoryImage,
        });
      } else if (event.target.name === "categoryBigIconURL") {
        this.setState({
          categoryBigIconURL: event.target.files[0],
        });
        const categoryImage = event.target.files[0];
        this.setState({
          categoryBigIconURL: categoryImage,
        });
      }
    }
  };

  componentWillMount() {
    this.props.loader({ show: true });
    categoryService.getAllCategories().then(
      (response) => {
        this.props.loader({ show: false });
        if (response.status === 200) {
          this.setState({
            allCategories: response.data.data,
          });
        }
      },
      (error) => {
        this.props.loader({ show: false });
        swal(
          "Oops.",
          "Sorry We Coudn't able to find any categories for you right now please try again later..",
          "error"
        );
        console.log(error);
        throw error;
      }
    );
  }

  handleActive = (event) => {
    this.props.loader({ show: true });
    let param = {
      categoryId: event.target.name,
      status: event.target.value === "true" ? false : true,
    };
    swal({
      title: "Are you sure?",
      text: "You Really Want To Change The Status Of This Category",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willUpdate) => {
      if (willUpdate) {
        categoryService.updateStatus(param).then(
          (response) => {
            this.props.loader({ show: false });
            if (response.status === 200) {
              swal("Sucess! Category Status Changed Succesfully!", {
                icon: "success",
              });
              this.componentWillMount();
            }
          },
          (error) => {
            this.props.loader({ show: false });
            swal(
              "Oops.",
              "Something Went Wrong. Please Try Again Later.",
              "error"
            );
            console.log(error);
          }
        );
      } else {
        this.props.loader({ show: false });
      }
    });
  };

  renderRedirect = () => {
    this.user = JSON.parse(sessionStorage.getItem("loginState"));
    if (this.user.role !== "admin") {
      return <Redirect to="/dashboard" />;
    }
  };
  render() {
    return (
      <div>
        {this.renderRedirect()}
        <div>
          <DashBoardHeader />
          <br></br>
          <br></br>
          <div className="dashboard">
            <Navbar />
            <div className="dashboard-content">
              <div id="titlebar">
                <div className="row">
                  <div className="col-md-12">
                    <h2>Manage Category</h2>

                    <nav id="breadcrumbs">
                      <ul>
                        <li>
                          <a href="#">Home</a>
                        </li>
                        <li>
                          <a href="#">Dashboard</a>
                        </li>
                        <li>Manage Category</li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12">
                  <form onSubmit={this.submitForm}>
                    <div className="add_listing_info">
                      {this.state.edit ? (
                        <h3>Edit Categories</h3>
                      ) : (
                        <h3>Add Categories</h3>
                      )}

                      <div className="form-group">
                        <label className="label-title">Category Name</label>
                        <input
                          type="text"
                          name="categoryName"
                          className="form-control"
                          ref={(ref) => (this.fileInput3 = ref)}
                          value={this.state.categoryName}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("categoryName")
                          }
                        />
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "categoryName",
                              this.state.categoryName,
                              "required|min:3|max:15"
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="label-title">
                          Category Description
                        </label>
                        <textarea
                          className="form-control"
                          name="categoryDescription"
                          ref={(ref) => (this.fileInput4 = ref)}
                          value={this.state.categoryDescription}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("categoryDescription")
                          }
                        ></textarea>
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "categoryDescription",
                              this.state.categoryDescription,
                              "required|min:15|max:150"
                            )}
                          </span>
                        </div>
                      </div>

                      {this.state.edit ? 
                        ''
                       : (
                        <div>
                          <div className="form-group">
                            <label className="label-title">
                              Category Small Icon Image
                            </label>
                            <input
                              type="file"
                              className="form-control"
                              name="categorySmallIconURL"
                              onChange={this.handleFileChange}
                              ref={(ref) => (this.fileInput = ref)}
                            ></input>
                            <div className="col-sm-12">
                              <span className="has-error">
                                {this.validator.message(
                                  "categorySmallIconURL",
                                  this.state.categorySmallIconURL,
                                  "required"
                                )}
                              </span>
                            </div>
                          </div>

                          <div className="form-group">
                            <label className="label-title">
                              Category Large Icon Image
                            </label>
                            <input
                              type="file"
                              className="form-control"
                              name="categoryBigIconURL"
                              ref={(ref) => (this.fileInput2 = ref)}
                              onChange={this.handleFileChange}
                            ></input>
                            <div className="col-sm-12">
                              <span className="has-error">
                                {this.validator.message(
                                  "categoryBigIconURL",
                                  this.state.categoryBigIconURL,
                                  "required"
                                )}
                              </span>
                            </div>
                          </div>
                        </div>
                      )}
                      {this.state.edit ? (
                        <button className="btn" onClick={this.updateForm}>
                          Edit Categories
                        </button>
                      ) : (
                        <button className="btn" onClick={this.submitForm}>
                          Add Categories
                        </button>
                      )}
                    </div>
                  </form>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12">
                  {this.state.allCategories === null ? (
                    <div className="add_listing_info">
                      <h3>
                        Sorry We Coundn't able to find any category for you
                        right now.
                      </h3>
                    </div>
                  ) : (
                    <div className="add_listing_info">
                      <h3>Category</h3>
                      <table className="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Category Name</th>
                            <th scope="col">Category Description</th>
                            <th colSpan="2" scope="col">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.allCategories.map((rowData) => (
                            <tr key={rowData.categoryId}>
                              <th scope="row">{rowData.categoryId}</th>
                              <td>{rowData.categoryName}</td>
                              <td>{rowData.categoryDescription}</td>
                              <td>
                                <button
                                  value={rowData.categoryActive}
                                  name={rowData.categoryId}
                                  onClick={this.handleActive}
                                  className={
                                    rowData.categoryActive
                                      ? "btn green"
                                      : "btn red"
                                  }
                                >
                                  {rowData.categoryActive
                                    ? "Active"
                                    : "InActive"}
                                </button>
                              </td>
                              <td>
                                <button
                                  className="btn"
                                  id={rowData.categoryId}
                                  onClick={this.onEdit}
                                >
                                  Edit
                                </button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <br></br>
            <div className="copyrights">
              Copyright &copy; 2020 TownHub. All Rights Reserved
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loader: (payload) => {
      dispatch(loader(payload));
    },
  };
};

export default connect(null, mapDispatchToProps)(ManageCategory);