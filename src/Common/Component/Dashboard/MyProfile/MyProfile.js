import React,{Component} from 'react';
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import '../dashboard.css';
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../../CategorySlider/noImage.jpg';
import loaderImage from '../../../../Loader/abc.gif'
import ProfileService from '../../../../Services/ProfileService'
import { Link } from 'react-router-dom';
class MyProfile extends Component{
    constructor(props){
        super(props)
        this.state={
            myProfile:null
        }
    }
    componentWillMount(){
        this.props.loader({show:true});
        let profile=JSON.parse(sessionStorage.getItem("loginState"))
        ProfileService.getProfileByProfileId(profile.id).then(
            (response) => {
                if (response.status === 200) {
  
                  this.setState({
                      myProfile:response.data.data
                  })
                  console.log(this.state.myProfile)
                  this.props.loader({show:false});
                }
              },
              (error) => {
                console.log(error);
                this.props.loader({show:false});
              }
            );
        
    }
    setErrorImage = (e) => {
        console.log("error")
        e.target.src = noImage;
    }
render()
   {
    let scrollPosition = 0;

            return(
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
            <div id="titlebar">
                <div className="row">
                    <div className="col-md-12">
                        <h2>My Profile</h2>
                
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Dashboard</a></li>
                                <li>My Profile</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
<div class="row">	
<div class="col-lg-12 col-md-12">
   { this.state.myProfile!==null?<div class="dashboard-list-box">
        <div class="user_image">
        <LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loaderImage} alt={'profile_image'} src={this.state.myProfile.profileImage!==null?this.state.myProfile.profileImage:"error.jpg"} scrollPosition={scrollPosition} effect="" height="150px" width="150px"/>
            <div class="buttons-to-right">
                <Link to="/dashboard/editprofile" class="button gray"><i class="fa fa-pencil"></i> Edit Profile</Link>
            </div>
        </div>
        <div class="user_info">
            <ul>
            <li><span>Full Name:</span> {this.state.myProfile.firstName} {this.state.myProfile.lastName}</li>
                <li><span>User Name:</span> {this.state.myProfile.userName}</li>
                <li><span>Email:</span> {this.state.myProfile.email}</li>
                <li><span>Phone:</span> {this.state.myProfile.phone}</li>
            </ul>
        </div>
    </div>:"Sorry We couldn't able to find any profile for you right now."}
</div>
</div>
</div>
</div>
</div>
            );
   }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}

export default connect(null, mapDispatchToProps)(trackWindowScroll(MyProfile));