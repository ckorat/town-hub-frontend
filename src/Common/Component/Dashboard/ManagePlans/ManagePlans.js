import React, { Component } from "react";
import plansService from "../../../../Services/PlansService";
import SimpleReactValidator from "simple-react-validator";
import DashBoardHeader from "../Header/DashBoardHeader";
import Navbar from "../Navbar/Navbar";
import "../dashboard.css";
import "./managePlans.css";
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from "sweetalert";
import {Redirect} from 'react-router-dom';
class ManagePlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allPlans: null,
      planName: "",
      planDescription: "",
      planAmount: "",
      planListingLimit: 0,
      planDuration: "",
      reviewShow: false,
      contactShow: false,
      edit: false,
    };
    this.validator = new SimpleReactValidator({ autoForceUpdate: this });
  }

  handleChange = (event) => {
    let change = {};
    change[event.target.name] = event.target.value;
    this.setState(change);
  };

  checkReview = () => {
    this.setState({
      reviewShow: !this.state.reviewShow,
    });
  };

  checkContact = () => {
    this.setState({
      contactShow: !this.state.contactShow,
    });
  };

  fetchData = () => {
    this.props.loader({ show: true });
    plansService.getAllPlans().then(
      (response) => {
        this.props.loader({ show: false });
        if (response.status === 200) {
          this.setState({
            allPlans: response.data.data,
          });
        }
      },
      (error) => {
        this.props.loader({ show: false });
        swal(
          "Oops.",
          "Sorry We Coudn't able to find any plans for you right now please try again later..",
          "error"
        );
        console.log(error);
        throw error;
      }
    );
  };

  componentWillMount() {
    this.fetchData();
  }

  submitForm = (event) => {
    event.preventDefault();
    this.props.loader({ show: true });
    if (this.validator.allValid()) {
      const data = {
        planName: this.state.planName,
        planDescription: this.state.planDescription,
        planAmount: this.state.planAmount,
        planListingLimit: this.state.planListingLimit,
        planDuration: this.state.planDuration,
        planActive: true,
        reviewShow: this.state.reviewShow,
        contactShow: this.state.contactShow,
      };
      plansService.addPlans(data).then(
        (response) => {
          if (response.status === 201) {
            swal("Success..", "Plan is Successfully added", "success");
            this.fetchData();
            console.log(response);
            this.props.history.push("/");
            this.props.history.push("/dashboard/manageplans");
            this.props.loader({ show: false });
          }
        },
        (error) => {
          this.props.loader({ show: false });
          swal(
            "Oops.",
            "Something Went Wrong. Please Try Again Later.",
            "error"
          );
          console.log(error);
        }
      );
    } else {
      this.props.loader({ show: false });
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  handleActive = (event) => {
    this.props.loader({ show: true });
    let param = {
      planId: event.target.name,
      status: event.target.value === "true" ? false : true,
    };
    swal({
      title: "Are you sure?",
      text: "You Really Want To Change The Status Of This Plan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willUpdate) => {
      if (willUpdate) {
        plansService.updateStatus(param).then(
          (response) => {
            this.props.loader({ show: false });
            if (response.status === 200) {
              swal("Success! Plan Status Changed Succesfully!", {
                icon: "success",
              });
              this.fetchData();
            }
          },
          (error) => {
            this.props.loader({ show: false });
            swal("OOPS! Something Went Wrong Please Try Again Later!", {
              icon: "error",
            });
            this.fetchData();
            throw error;
          }
        );
      } else {
        this.props.loader({ show: false });
      }
    });
  }

  onEdit = (event) => {
    let id = event.target.id;
    event.preventDefault();
    let fetched_data = this.state.allPlans.filter(
      (data) => data.planId === +id
    );

    if (fetched_data) {
      this.setState(
        {
          edit: true,
          planId: id,
          planName: fetched_data[0].planName,
          planDescription: fetched_data[0].planDescription,
          planAmount: fetched_data[0].planAmount,
          planDuration: fetched_data[0].planDuration,
          planListingLimit: fetched_data[0].planListingLimit,
          planActive: fetched_data[0].planActive,
          reviewShow: fetched_data[0].reviewShow,
          contactShow: fetched_data[0].contactShow,
        },
        () => {
          console.log("data:", fetched_data);
        }
      );
    }
  };

  updateForm = (event) => {
    event.preventDefault();
    this.props.loader({ show: true });
    if (this.validator.allValid()) {
      const data = {
        planId: this.state.planId,
        planName: this.state.planName,
        planDescription: this.state.planDescription,
        planAmount: this.state.planAmount,
        planListingLimit: this.state.planListingLimit,
        planDuration: this.state.planDuration,
        planActive: this.state.planActive,
        reviewShow: this.state.reviewShow,
        contactShow: this.state.contactShow,
      };
       swal({
      title: "Are you sure?",
      text: "You Really Want To Edit This Plan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willUpdate) => {
      if (willUpdate) {
      plansService.updatePlans(data).then(
        (response) => {
          if (response.status === 200) {
            swal("Success..", "Plan is Successfully updated", "success");
            this.fetchData();
            console.log(response);
            this.props.history.push("/");
            this.props.history.push("/dashboard/manageplans");
            this.props.loader({ show: false });
          }
        },
        (error) => {
          this.props.loader({ show: false });
          swal(
            "Oops.",
            "Something Went Wrong. Please Try Again Later.",
            "error"
          );
          console.log(error);
        }
      );
    } else {
      this.props.loader({ show: false });
      this.validator.showMessages();
      this.forceUpdate();
    }
    });
    }
    else {
      this.props.loader({ show: false });
      this.validator.showMessages();
      this.forceUpdate();
    }
  };
  renderRedirect = () => {
    this.user=JSON.parse(sessionStorage.getItem("loginState"))
    if (this.user.role!=="admin") {
      return <Redirect to='/dashboard' />
    }
  }
  render() {
    return (
      <div>
        {this.renderRedirect()}
        <div>
          <DashBoardHeader />
          <br></br>
          <br></br>
          <div className="dashboard">
            <Navbar />
            <div className="dashboard-content">
              <div id="titlebar">
                <div className="row">
                  <div className="col-md-12">
                    <h2>Manage Plans</h2>

                    <nav id="breadcrumbs">
                      <ul>
                        <li>
                          <a href="#">Home</a>
                        </li>
                        <li>
                          <a href="#">Dashboard</a>
                        </li>
                        <li>Manage Plans</li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12">
                  <form onSubmit={this.submitForm}>
                    <div className="add_listing_info">
                    {this.state.edit ? (
                        <h3>Edit Plans</h3>
                      ) : (
                        <h3>Add Plans</h3>
                      )}
                      
                      <div className="form-group">
                        <label className="label-title">Plan Name</label>
                        <input
                          type="text"
                          name="planName"
                          className="form-control"
                          value={this.state.planName}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("planName")
                          }
                        />
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "planName",
                              this.state.planName,
                              "required|min:3|max:15"
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="label-title">Plan Description</label>
                        <textarea
                          className="form-control"
                          name="planDescription"
                          value={this.state.planDescription}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("planDescription")
                          }
                        ></textarea>
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "planDescription",
                              this.state.planDescription,
                              "required|min:15|max:200"
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="label-title">Plan Amount</label>
                        <input
                          type="text"
                          className="form-control"
                          name="planAmount"
                          value={this.state.planAmount}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("planAmount")
                          }
                        ></input>
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "planAmount",
                              this.state.planAmount,
                              "required"
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="label-title">Listing Limit</label>
                        <input
                          type="number"
                          className="form-control"
                          name="planListingLimit"
                          value={this.state.planListingLimit}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("planListingLimit")
                          }
                        ></input>
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "planListingLimit",
                              this.state.planListingLimit,
                              "required"
                            )}
                          </span>
                        </div>
                      </div>

                      <div className="form-group">
                        <label className="label-title">Plan Duration</label>
                        <input
                          type="text"
                          className="form-control"
                          name="planDuration"
                          value={this.state.planDuration}
                          onChange={this.handleChange}
                          onBlur={() =>
                            this.validator.showMessageFor("planDuration")
                          }
                        ></input>
                        <div className="col-sm-12">
                          <span className="has-error">
                            {this.validator.message(
                              "planDuration",
                              this.state.planDuration,
                              "required"
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="form-group">
                        <div>
                          <input
                            type="checkbox"
                            checked={this.state.reviewShow ? true : false}
                            onClick={this.checkReview}
                            id="test1"
                          />
                          <label htmlFor="test1">Show Review</label>
                        </div>
                      </div>
                      <div className="form-group">
                        <div>
                          <input
                            type="checkbox"
                            checked={this.state.contactShow ? true : false}
                            onClick={this.checkContact}
                            id="test2"
                          />
                          <label htmlFor="test2">Show Contact</label>
                        </div>
                      </div>

                      {this.state.edit ? (
                        <button className="btn" onClick={this.updateForm}>
                          Edit Plan
                        </button>
                      ) : (
                        <button className="btn" onClick={this.submitForm}>
                          Add Plan
                        </button>
                      )}
                    </div>
                  </form>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12">
                  {this.state.allPlans === null ? (
                    <div className="add_listing_info">
                      <h3>
                        Sorry We Coundn't able to find any plan for you right
                        now.
                      </h3>
                    </div>
                  ) : (
                    <div className="add_listing_info">
                      <h3>Plan</h3>
                      <table className="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Plan Name</th>
                            <th scope="col">Plan Description</th>
                            <th scope="col">Plan Amount</th>
                            <th scope="col">Plan Duration</th>
                            <th scope="col">Listing Limit</th>
                            <th colSpan="2" scope="col">
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.allPlans.map((rowData, index) => (
                            <tr key={rowData.planId}>
                              <th scope="row">{index + 1}</th>
                              <td>{rowData.planName}</td>
                              <td>{rowData.planDescription}</td>
                              <td>{rowData.planAmount}</td>
                              <td>{rowData.planDuration}</td>
                              <td>{rowData.planListingLimit}</td>
                              <td>
                                <button
                                  value={rowData.planActive}
                                  name={rowData.planId}
                                  onClick={this.handleActive}
                                  className={
                                    rowData.planActive ? "btn green" : "btn red"
                                  }
                                >
                                  {rowData.planActive ? "Active" : "InActive"}
                                </button>
                              </td>
                              <td>
                                <button
                                  id={rowData.planId}
                                  onClick={this.onEdit}
                                  className="btn"
                                >
                                  Edit
                                </button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <br></br>
            <div className="copyrights">
              Copyright &copy; 2020 TownHub. All Rights Reserved
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loader: (payload) => {
      dispatch(loader(payload));
    },
  };
};
export default connect(null, mapDispatchToProps)(ManagePlans);