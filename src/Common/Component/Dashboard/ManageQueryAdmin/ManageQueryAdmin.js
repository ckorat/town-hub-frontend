import React, {Component} from 'react'
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import QueryService from '../../../../Services/QueryService'
import MessageModel from '../MessageModel/MessageModel';
class ManageQueryAdmin extends Component{
    constructor(props){
        super(props)
        this.state={
            isMessageOpen:false,
            allQueries:null
        }
        this.handleMessageBox=this.handleMessageBox.bind(this);
    }
    handleMessageBox(){
        this.setState({
            isMessageOpen:!this.state.isMessageOpen
        })
    }

    componentWillMount(){
        this.props.loader({show:true})
        QueryService.getQuery().then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        allQueries: response.data.data
                    })
                    console.log(response.data.data)
                }
            },
            (error) => {
                this.props.loader({show:false});
                swal("Oops.", "Sorry We Coudn't able to find any Query for you right now please try again later..", "error");
                console.log(error)
                throw error;
            }
        )
    }
    renderRedirect = () => {
        this.user=JSON.parse(sessionStorage.getItem("loginState"))
        if (this.user.role!=="admin") {
          return <Redirect to='/dashboard' />
        }
      }
    render(){
        return(
            <div>
                {this.renderRedirect()}
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
            <div id="titlebar">
                <div className="row">
                    <div className="col-md-12">
                            <h2>Manage Query</h2>
                
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Dashboard</a></li>
                                <li>Manage Query</li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="row">
            <div className="col-lg-12 col-md-12">
            {this.state.allQueries===null?
                <div className="add_listing_info">
                    <h3>Sorry We Coundn't able to find any Queries for you right now.</h3>
                </div>:
                <div className="add_listing_info">
                 <h3>Manage Queries</h3>	
                 <table className="table table-hover table-responsive">
       <thead>
         <tr>
           <th scope="col"># Query ID</th>
           <th scope="col">Full Name</th>
           <th scope="col">Title</th>
           <th scope="col">Email</th>
           <th scope="col">Phone Number</th>
           <th scope="col">Action</th>
         </tr>
       </thead>
       <tbody>
       {this.state.allQueries.filter(data=>data.listingId===0).map((rowData) => (
                 <tr key={rowData.queryId}>
                     <th scope="row">{rowData.queryId}</th>
                        <td>{rowData.firstName} {rowData.lastName}</td>
                        <td>{rowData.title}</td>
                        <td>{rowData.email}</td>
                        <td>{rowData.phoneNumber}</td>
                        <td><button onClick={this.handleMessageBox} className="btn grey">Reply</button></td>
                       
                 </tr>

))}
        
       </tbody>
     </table>
     </div>}
                
                </div>
                </div>
            </div>
        </div>
        <br></br> 
        {this.state.isMessageOpen?<MessageModel action={this.handleMessageBox}/>:null}
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
        </div>
        </div>
        </div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ManageQueryAdmin);