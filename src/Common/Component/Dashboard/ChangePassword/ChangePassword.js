import React, {Component} from 'react'
import SimpleReactValidator from 'simple-react-validator';
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import ProfileService from '../../../../Services/ProfileService'

class ChangePassword extends Component{
    constructor(props){
        super(props)
        this.state={
            oldPassword:'',
            newPassword:'',
            confirmPassword:'',
            cnfpasswordError:false,
            isOldPasswordError:false
        }
        this.validator = new SimpleReactValidator({autoForceUpdate: this});
        this.checkPassword=this.checkPassword.bind(this);
    }
    handleChange = event => {
        let change = {}
        change[event.target.name] = event.target.value
        this.setState(change)
  };
  checkPassword(event){
    this.handleChange(event)
    console.log(event)
    if(!this.validator.fieldValid('confirmPassword')){
          this.validator.showMessageFor('confirmPassword')
          this.forceUpdate();
    }
    if(this.state.newPassword!==this.state.confirmPassword){
      this.setState({
          cnfpasswordError:true
      })
  }else{
      this.setState({
          cnfpasswordError:false
      })
  }
}
submitForm = (event)=>{
    event.preventDefault();
    this.props.loader({show:true});
    if (this.validator.allValid()) {
        if(this.state.newPassword===this.state.confirmPassword){
            let user=JSON.parse(sessionStorage.getItem("loginState"))
            let data={
                id:user.id,
                newPassword:this.state.newPassword,
                oldPassword:this.state.oldPassword
            }
            swal({
                title: "Are you sure?",
                text: "You Really Want To Change Your Password?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willUpdate) => {
                if (willUpdate) {
            ProfileService.updatePassword(data).then(
              (response)=>{
                  this.props.loader({show:false});
                  swal("Password Update", "Your Password is channged succesfully please login to continue..", "success");
                  sessionStorage.clear();
                  this.props.history.push("/");
              },
              (error)=>{
                  if(error.status===409){
                      this.props.loader({show:false});
                      console.log(error)
                      this.setState({
                          isOldPasswordError:true
                      })
                  }
                  else{
                      this.setState({
                        isOldPasswordError:false
                      })
                      this.props.loader({show:false});
                      console.log(error);
                      swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                  }
                  
              }
            )
        }else{
            this.props.loader({show:false})
          }
      });
        }else{
            this.setState({
              cnfpasswordError:true
            })
            this.props.loader({show:false});
        }
      
      
  }
    else{
      this.props.loader({show:false});
      this.validator.showMessages();
      this.forceUpdate();
    }
}
    render(){
        return(
 <div>
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
            <div id="titlebar">
                <div className="row">
                    <div className="col-md-12">
                        <h2>Manage Password</h2>
                
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Dashboard</a></li>
                                <li>Change Password</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div className="row">
            <div className="col-lg-12 col-md-12">
            <form onSubmit={this.submitForm}>
                <div className="add_listing_info">
                            <h3>Change Password</h3>				
                            <div className="form-group">
                                    <label className="label-title">Old Password</label>
                                    <input type="password" name="oldPassword" className="form-control" ref={ref=> this.fileInput3 = ref} value={this.state.oldPassword} onChange={this.handleChange}
            onBlur={() => this.validator.showMessageFor('oldPassword')}/>
            {this.state.isOldPasswordError?<div>
                <span className="has-error">Your Old Password Is Wrong. Please Insert Correct Password.</span>
            </div>:null}
            <div className="col-sm-12">
                <span className="has-error">{this.validator.message('oldPassword', this.state.oldPassword, 'required|min:6|max:15')}</span>
            </div>
             
                                </div>
                                <div className="form-group">
                                    <label className="label-title">New Password</label>
                                    <input type="password" name="newPassword" className="form-control" ref={ref=> this.fileInput3 = ref} value={this.state.newPassword} onChange={this.handleChange}
            onBlur={() => this.validator.showMessageFor('newPassword')}/>
            <div className="col-sm-12">
                <span className="has-error">{this.validator.message('newPassword', this.state.newPassword, 'required|min:6|max:15')}</span>
            </div>
             
                                </div>
                                <div className="form-group">
                                    <label className="label-title">Confirm New Password</label>
                                    <input type="password" name="confirmPassword" className="form-control" ref={ref=> this.fileInput3 = ref} value={this.state.confirmPassword} onChange={this.handleChange}
            onBlur={this.checkPassword}/>
            {this.state.cnfpasswordError?<div className="col-sm-12">
                <span className="has-error">Confirm Password Should Be Same.</span>
            </div>:null}
            <div className="col-sm-12">
                <span className="has-error">{this.validator.message('confirmPassword', this.state.confirmPassword, 'required')}</span>
            </div>
             
                                </div>
                            
                           
                           
                            <button className="btn" onClick={this.submitForm}>Change Password</button>                
                        </div>
                        
                        </form>
            </div>
            </div>
            </div>
                <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
			
            </div>
        </div>
            </div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ChangePassword);