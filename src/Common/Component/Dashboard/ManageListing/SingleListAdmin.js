import React,{Component} from 'react'
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../../CategorySlider/noImage.jpg';
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import ListingService from '../../../../Services/ListingService'
import loaderImage from '../../../../Loader/abc.gif'
class SingleListAdmin extends Component{
	constructor(props){
        super(props)
        
	}
	setErrorImage = (e) => {
        console.log("error")
        e.target.src = noImage;
	}
	
    render(){
		let scrollPosition = 0;
        return(
            <li>
        <div className="list-box-listing">
			<div className="list-box-listing-img"><LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loaderImage} alt={'listing_image'} src={this.props.data.listingImage!==null?this.props.data.listingImage:"error.jpg"} scrollPosition={scrollPosition} effect=""/></div>
								<div className="list-box-listing-content">
									<div className="inner">
										<h3><a href="#">{this.props.data.listingName}</a></h3>
										<span>{this.props.data.listingTagLine}</span><br></br>
										<i className="fa fa-map-marker"></i><span>{this.props.data.city}</span>
										<div className="star-rating">
											<div className="rating-counter">(12 reviews)</div>
										</div>
									</div>
								</div>
							</div>
							<div className="buttons-to-right">
								<button value={this.props.data.listingVerify} name={this.props.data.listingId} onClick={this.props.onClick} className={this.props.data.listingVerify?"button green":"button red"}><i className="fa fa-pencil"></i> {this.props.data.listingVerify?"Verrified":"Not Verified"}</button>
							</div>
        </li>
        )
    }
}const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(trackWindowScroll(SingleListAdmin));