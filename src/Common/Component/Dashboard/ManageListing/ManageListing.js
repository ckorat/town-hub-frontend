import React,{Component} from 'react'
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import ListingService from '../../../../Services/ListingService'
import SingleListAdmin from './SingleListAdmin';
class ManageListing extends Component{
    constructor(props){
        super(props)
        this.state={
            myListing:null
        }
        this.handleActive=this.handleActive.bind(this)
    }
    componentWillMount(){
        this.props.loader({show:true});
        ListingService.getAllListing().then(
            (response) => {
                if (response.status === 200) {
  
                  this.setState({
                      myListing:response.data.data
                  })
                  console.log(this.state.myListing)
                  this.props.loader({show:false});
                }
              },
              (error) => {
                console.log(error);
                this.props.loader({show:false});
              }
            );
        
    }
    handleActive(event){
        this.props.loader({show:true});
        let param={
            field:"listingVerify",
            id:event.target.name,
            status:event.target.value==="true"?false:true
        }
        swal({
            title: "Are you sure?",
            text: "You Really Want To Change The Status Of This Listing?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willUpdate) => {
            if (willUpdate) {
                ListingService.updateStatus(param).then(
                    (response)=>{
                        this.props.loader({show:false});
                        if(response.status===200)
                        {
                            swal("Sucess! Listing Status Changed Succesfully!", {
                                icon: "success",
                              });
                            this.componentWillMount()
                        }
                    },
                    (error)=>{

                        this.props.loader({show:false});
                        swal("OOPS! Something Went Wrong Please Try Again Later!", {
                            icon: "error",
                          });
                        this.componentWillMount()
                        throw error;
                    }
                )
              }else{
                this.props.loader({show:false})
              }
          });
    }
    render(){
        return(
            <div>
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
                <div id="titlebar">
			<div className="row">
				<div className="col-md-12">
					<h2>Manage Listings</h2>
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Dashboard</a></li>
							<li>Manage Listings</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>{this.state.myListing!==null?
        <div className="row">
        <div className="col-lg-12 col-md-12">
        <div className="dashboard-list-box">
            
        <h4>Manage Listings</h4>
        <ul>
        {this.state.myListing?.map((rowData) => (
            
            <SingleListAdmin data={rowData} onClick={this.handleActive}/>
            ))}
        </ul>
        </div>
        </div>
        </div>:<div className="row">
        <div className="col-lg-12 col-md-12">
        <div className="dashboard-list-box">
            
        <h4>No Listing Is avaialable right now. Please try again later..</h4>
        </div>
        </div>
        </div>}
            </div>
            <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
			
            </div>
        </div>
            </div>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ManageListing);