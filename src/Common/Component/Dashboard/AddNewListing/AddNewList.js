import React, {Component} from 'react';
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
import categoryService from '../../../../Services/categoryService';
import { loader } from "../../../../Redux/Actions";
import SimpleReactValidator from 'simple-react-validator';
import { connect } from "react-redux";
import swal from 'sweetalert';
import UtilsService from '../../../../Services/UtilsService'
import ListingService from '../../../../Services/ListingService'
import {storage} from '../../../../Firebase/index';
import { Redirect } from 'react-router-dom';
class AddNewList extends Component{
    constructor(props){
        super(props)
        this.state={
            listingTitle:'',
            listingTagLine:'',
            description:'',
            categoryName:'',
            addressId:'',
            contactId:'',
            addressLine1:'',
            addressLine2:'',
            businessId:'',
            area:'',
            city:'',
            state:'',
            country:'',
            pincode:'',
            phonenumber:'',
            email:'',
            worknumber:'',
            website:'',
            minPrice:'',
            maxPrice:'',
            mondayOpen:'',
            mondayClose:'',
            tuesdayOpen:'',
            tuesdayClose:'',
            wednesdayOpen:'',
            wednesdayClose:'',
            thursdayOpen:'',
            thursdayClose:'',
            fridayOpen:'',
            fridayClose:'',
            saturdayOpen:'',
            saturdayClose:'',
            sundayOpen:'',
            sundayClose:'',
            allCategories:null
        }
        this.validator2 = new SimpleReactValidator({autoForceUpdate: this});
        this.handleFileChange=this.handleFileChange.bind(this);
    }
    componentWillMount(){
        this.props.loader({show:true});
        categoryService.getAllCategories().then(
            (response) => {
                if (response.status === 200) {
                    this.setState({
                        allCategories: response.data.data
                    }) 
                    this.props.loader({show:false});
                    console.log(this.state.allCategories)
                }
            },
            (error) => {
                this.props.loader({show:false});
                console.log(error)
                throw error;
            }
        )
    }
    handleChange = event => {
		let change = {}
        change[event.target.name] = event.target.value
        
		this.setState(change)
  };
  handleFileChange = event => {
    if(event.target.files[0]){
        this.setState({
            listingImage:event.target.files[0]
        })
    }
}
submitForm = (event)=>{
    event.preventDefault();
    this.user=JSON.parse(sessionStorage.getItem("loginState"))
    this.setState({
        businessId:this.user.id
    })
    this.props.loader({show:true});
        if (this.validator2.allValid()) {
            const uploadTask = storage.ref(`listing/${this.state.listingTitle}/${this.state.listingImage.name}`).put(this.state.listingImage);
            uploadTask.on('state_changed',(snapshot)=>{
                this.setState({
                    progress:Math.round((snapshot.bytesTransferred/snapshot.totalBytes)*100)
                })
    
            }, (error)=>{
            console.log(error);
        }, ()=>{
            storage.ref(`listing/${this.state.listingTitle}`).child(this.state.listingImage.name).getDownloadURL().then(downloadurl=>{
                this.setState({
                    listingImageUrl:downloadurl
                })
                const data={
                    address:{
                        addressLine1:this.state.addressLine1,
                        addressLine2:this.state.addressLine2,
                        area:this.state.area,
                        city:this.state.city,
                        state:this.state.state,
                        country:this.state.country,
                        pinCode:this.state.pincode
                    },
                    contact:{
                    email:this.state.email,
                      phoneNumber:this.state.phonenumber,
                      workNumber:this.state.worknumber
                    }
                }
                UtilsService.saveAddress(data.address).then(
                    (response)=>{
                        console.log(response)
                        this.setState({
                            addressId:response.data.data
                        })
                        UtilsService.saveContact(data.contact).then(
                            (response)=>{
                                console.log(response)
                                this.setState({
                                    contactId:response.data.data
                                })
                                const listing={
                                    listingName:this.state.listingTitle,
                                    listingTagLine:this.state.listingTagLine,
                                    listingDescription:this.state.description,
                                    categoryId:this.state.categoryName,
                                    profileId:this.state.businessId,
                                    addressId:this.state.addressId,
                                    contactId:this.state.contactId,
                                    listingImage:this.state.listingImageUrl,
                                    minPrice:this.state.minPrice,
                                    maxPrice:this.state.maxPrice,
                                    website:this.state.website,
                                    listingActive:true,
                                    listingVerify:false,
                                }
                                ListingService.addListing(listing).then(
                                    (response)=>{
                                        console.log(response.data.data)
                                        const listingTime={
                                            listingId:response.data.data,
                                            mondayOpen:this.state.mondayOpen,
                                            mondayClose:this.state.mondayClose,
                                            tuesdayOpen:this.state.tuesdayOpen,
                                            tuesdayClose:this.state.tuesdayClose,
                                            wednesdayOpen:this.state.wednesdayOpen,
                                            wednesdayClose:this.state.wednesdayClose,
                                            thursdayOpen:this.state.thursdayOpen,
                                            thursdayClose:this.state.thursdayClose,
                                            fridayOpen:this.state.fridayOpen,
                                            fridayClose:this.state.fridayClose,
                                            saturdayOpen:this.state.saturdayOpen,
                                            saturdayClose:this.state.saturdayClose,
                                            sundayOpen:this.state.sundayOpen,
                                            sundayClose:this.state.sundayClose,
                                        }
                                        ListingService.addListingTime(listingTime).then(
                                            (response)=>{
                                                console.log(response)
                                                this.props.loader({show:false});
                                                swal("Success..","Your Listing Submitted Succesfully.","success")
                                                this.props.history.push("/dashboard")
                                            },
                                            (error)=>{
                                                this.props.loader({show:false});
                                                console.log(error);
                                                swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                                            }
                                        )
                                    },
                                    (error)=>{
                                            this.props.loader({show:false});
                                            console.log(error);
                                            swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                                    }
                                  )
                            },
                            (error)=>{
                                    this.props.loader({show:false});
                                    console.log(error);
                                    swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                            }
                          )
                    },
                    (error)=>{
                            this.props.loader({show:false});
                            console.log(error);
                            swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                    }
                  )
            })
        });
        
        } else {
        this.props.loader({show:false});
          this.validator2.showMessages();
          this.forceUpdate();
        }
    
}
renderRedirect = () => {
    this.user=JSON.parse(sessionStorage.getItem("loginState"))
    if (this.user.role!=="business") {
      return <Redirect to='/dashboard' />
    }
  }
    render(){
        return(
            
            <div>
        {this.renderRedirect()}

                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
                <div id="titlebar">
			        <div className="row">
				        <div className="col-md-12">
					        <h2>Add New Listing</h2>
					
					        <nav id="breadcrumbs">
						        <ul>
							        <li><a href="#">Home</a></li>
							        <li><a href="#">Dashboard</a></li>
							        <li>Add New Listing</li>
						        </ul>
					        </nav>
				        </div>
			        </div>
		        </div>
        <div className="row">
			<div className="col-lg-12 col-md-12">
					<form onSubmit={this.submitForm}>
                    	<div className="add_listing_info">
                            <h3>Basic Informations</h3>				
                            <div className="form-group">
                                    <label className="label-title">Listing Title</label>
                                    <input type="text" className="form-control" placeholder="Listing Title" name="listingTitle" value={this.state.listingTitle} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('listingTitle') }></input>
                                    <div>
                                        <span className="has-error">{this.validator2.message('listingTitle', this.state.listingTitle, 'required|min:3')}</span>
                                    </div>
                                </div>
                            <div className="form-group">
                                <label className="label-title">Listing Tag Line</label>
                                <input type="text" className="form-control" placeholder="Listing Tag Line" name="listingTagLine" value={this.state.listingTagLine} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('listingTagLine') }></input>
                                    <div>
                                        <span className="has-error">{this.validator2.message('listingTagLine', this.state.listingTitle, 'required|min:3')}</span>
                                    </div>
                                </div>
                            <div className="form-group">
                                <label className="label-title">Category</label>
                                <div className="select">
                                    <select className="form-control" placeholder="Select Category" name="categoryName" value={this.state.categoryName} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('categoryName') }>
                                    <option value="0">What are you looking for?</option>
                            {this.state.allCategories?.map((rowData) => (
                                <option value={rowData.categoryId}>{rowData.categoryName}</option>
                            ))}
                                    </select>
                                </div>
                                    <div>
                                        <span className="has-error">{this.validator2.message('categoryName', this.state.categoryName, 'required')}</span>
                                    </div>
                            </div>                   
                        </div> 
                        <div className="add_listing_info">
                            <h3>Listing Details</h3>				
                            <div className="form-group">
                                <label className="label-title">Description</label>
                                <textarea className="form-control" placeholder="Listing Description" name="description" value={this.state.description} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('description') }></textarea>
                                <div>
                                        <span className="has-error">{this.validator2.message('description', this.state.description, 'required|min:3')}</span>
                                    </div>
                            </div>  
                                     
                        </div>
                        <div className="add_listing_info">
                            <h3>Address Info</h3>				
                            <div className="form-group">
                                <label className="label-title">Address Line 1</label>
                                <input type="text" className="form-control" placeholder="Address Line 1" name="addressLine1" value={this.state.addressLine1} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('addressLine1') }></input>
                                <div>
                                     <span className="has-error">{this.validator2.message('addressLine1', this.state.addressLine1, 'required|min:3|max:80')}</span>
                                    </div>
                            </div>
                            <div className="form-group">
                                <label className="label-title">Address Line 2</label>
                                <input type="text" className="form-control" placeholder="Address Line 2" name="addressLine2" value={this.state.addressLine2} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('addressLine2') }></input>
                                <div>
                                     <span className="has-error">{this.validator2.message('addressLine2', this.state.addressLine2, 'min:3|max:80')}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-sm-6">
                                    <label className="label-title">Area</label>
                                    
                                    <input type="text" className="form-control" placeholder="Area" name="area" value={this.state.area} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('area') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('area', this.state.area, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className="label-title">City</label>
                                    <input type="text" className="form-control" placeholder="City" name="city" value={this.state.city} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('city') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('city', this.state.city, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-sm-6">
                                    <label className="label-title">State</label>
                                    <input type="text" className="form-control" placeholder="State" name="state" value={this.state.state} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('state') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('state', this.state.state, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>  
                                <div className="form-group col-sm-6">
                                    <label className="label-title">Country</label>
                                    <input type="text" className="form-control" placeholder="Country" name="country" value={this.state.country} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('country') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('country', this.state.country, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>  
                                <div className="form-group col-sm-12">
                                    <label className="label-title">Pin Code</label>
                                    <input type="text" className="form-control" placeholder="Pin Code" name="pincode" value={this.state.pincode} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('pincode') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('pincode', this.state.pincode, 'required|min:6|max:6|numeric')}</span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div className="add_listing_info">
                            <h3>Contact Info</h3>
                           
                            <div className="form-group">
                                    <label className="label-title">Phone Number</label>
                                    <input type="text" className="form-control" placeholder="Phone Number" name="phonenumber" value={this.state.phonenumber} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('phonenumber') }></input>
                                    <div>
                <span className="has-error">{this.validator2.message('phonenumber', this.state.phonenumber, 'required|min:10|max:10|phone')}</span>
            </div>
                                </div>
	                            <div className="form-group">
                                    <label className="label-title">Email</label>
                                    <input type="text" className="form-control" placeholder="Email" name="email" value={this.state.email} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('email') }></input>
                                    <div>
                <span className="has-error">{this.validator2.message('email', this.state.email, 'required|email')}</span>
            </div>
                                </div>
                                <div className="form-group">
                                    <label className="label-title">Website</label>
                                    <input type="text" className="form-control" placeholder="Website" name="website" value={this.state.website} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('website') }></input>
                                    <div>
                <span className="has-error">{this.validator2.message('website', this.state.website, 'required|url')}</span>
            </div>
                                </div>
                                <div className="form-group">
                                    <label className="label-title">Work Number</label>
                                    <input type="text" className="form-control" placeholder="Work Number" name="worknumber" value={this.state.worknumber} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('worknumber')}></input>
                                    <div>
                                        <span className="has-error">{this.validator2.message('worknumber', this.state.worknumber, 'min:10|max:10|phone')}</span>
                                    </div>
                                </div>
                                      
                        </div>
                        
                        
                        
                        <div className="add_listing_info">
                            <h3>Opening Hours</h3>			
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Monday</label>
                                </div>
                                
                                <div className="form-group col-sm-5">    
                                    <input type="time" className="form-control" name="mondayOpen" value={this.state.mondayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                    <input type="time" className="form-control" name="mondayClose" value={this.state.mondayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Tuesday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="tuesdayOpen" value={this.state.tuesdayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="tuesdayClose" value={this.state.tuesdayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Wednesday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="wednesdayOpen" value={this.state.wednesdayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="wednesdayClose" value={this.state.wednesdayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Thrusday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="thursdayOpen" value={this.state.thursdayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="thursdayClose" value={this.state.thursdayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Friday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="fridayOpen" value={this.state.fridayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="fridayClose" value={this.state.fridayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Saturday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="saturdayOpen" value={this.state.saturdayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="saturdayClose" value={this.state.saturdayClose} onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row">  
                                <div className="form-group col-sm-2">
                                    <label className="label-title">Sunday</label>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="sundayOpen" value={this.state.sundayOpen} onChange={this.handleChange}/>
                                </div>
                                <div className="form-group col-sm-5">    
                                <input type="time" className="form-control" name="sundayClose" value={this.state.sundayClose} onChange={this.handleChange}/>
                                </div>
                            </div>         
                        </div>  
                        
                        <div className="add_listing_info">
                            <h3>Add Pricing</h3>	
                            <div className="row">			
                                <div className="form-group col-sm-6">
                                    <label className="label-title">Min. Price</label>
                                    <input type="text" className="form-control" placeholder="Min Price" name="minPrice" value={this.state.minPrice} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('minPrice')}></input>
                                    <div>
                <span className="has-error">{this.validator2.message('minPrice', this.state.minPrice, 'required|numeric')}</span>
            
                                </div>
                                </div>
                                 <div className="form-group col-sm-6">
                                    <label className="label-title">Max. Price</label>
                                    <input type="text" className="form-control" placeholder="Max Price" name="maxPrice" value={this.state.maxPrice} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('maxPrice')}></input>
                                    <div>
                <span className="has-error">{this.validator2.message('maxPrice', this.state.maxPrice, 'required|numeric')}</span>
            
                                </div>
                                </div>
                            </div>
                                     
                        </div>   
                        <div className="add_listing_info">
                            <h3>Add Gallery Images</h3>				
                            <div className="form-group">
                                    <label className="label-title">Listing Image</label>
                                    <input type="file" className="form-control" name="listingImage" onChange={this.handleFileChange} ref={ref=> this.fileInput = ref}></input>
                                    <div className="col-sm-12">
                                        <span className="has-error">{this.validator2.message('listingImage', this.state.listingImage, 'required')}</span>
                                    </div>
                                </div>
                        </div>
                        <div className="add_listing_info">
                            <button className="btn" onClick={this.submitForm}>Add Listing</button>
                        </div>   
                    </form>
			</div>


			
			</div>
		</div>
        <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
			
            </div>
        </div>
            </div>
            
        )
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(AddNewList);