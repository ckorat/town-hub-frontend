import React,{Component} from 'react'
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../../CategorySlider/noImage.jpg';
import loaderImage from '../../../../Loader/abc.gif'
class SingleList extends Component{
	constructor(props){
		super(props)
	}
	setErrorImage = (e) => {
        console.log("error")
        e.target.src = noImage;
	}
	
    render(){
		let scrollPosition = 0;
        return(
            <li>
        <div className="list-box-listing">
			<div className="list-box-listing-img"><LazyLoadImage onError={this.setErrorImage.bind(this)} placeholderSrc={loaderImage} alt={'listing_image'} src={this.props.data.listingImage!==null?this.props.data.listingImage:"error.jpg"} scrollPosition={scrollPosition} effect=""/></div>
								<div className="list-box-listing-content">
									<div className="inner">
										<h3><a href="#">{this.props.data.listingName}</a></h3>
										<span>{this.props.data.listingTagLine}</span><br></br>
										<i className="fa fa-map-marker"></i><span>{this.props.data.city}</span>
										<div className="star-rating">
											<div className="rating-counter">(12 reviews)</div>
										</div>
									</div>
								</div>
							</div>
							<div className="buttons-to-right">
								<a href="#" className="button gray"><i className="fa fa-pencil"></i> Edit</a>
                                <button value={this.props.data.listingActive} name={this.props.data.listingId} onClick={this.props.onClick} className={this.props.data.listingActive?"button green":"button red"}><i className="fa fa-pencil"></i> {this.props.data.listingActive?"Active":"InActive"}</button>
							</div>
        </li>
        )
    }
}
export default trackWindowScroll(SingleList)