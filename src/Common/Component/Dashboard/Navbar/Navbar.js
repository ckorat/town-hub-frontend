import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';
class Navbar extends Component{
    constructor(props){
        super(props)
        this.state={
            myProfile:null
        }
        this.logout=this.logout.bind(this)
    }
    logout(){
        sessionStorage.clear()
        swal("You're Logged Out.", "Thank You For Using Our Services. We love to see you again soon..", "success");

    }
    returnMenu= () =>{
        switch(this.state.myProfile.role){
            case "admin":
                return(
                    <ul>
                    <li className="active"><Link to="/dashboard" ><i className="fa fa-cogs"></i> Dashboard</Link></li>
                    <li><Link to="/dashboard/myprofile"><i className="fa fa-user-o"></i> My Profile</Link></li>
                    <li><Link to="/dashboard/changepassword"><i className="fa fa-user-o"></i> Change Password</Link></li>
                    <li><Link to="/dashboard/managecategory"><i className="fa fa-list-alt "></i> Manage Category</Link></li>
                    <li><Link to="/dashboard/manageplans"><i className="fa fa-list-alt "></i> Manage Plans</Link></li>
                    <li><Link to="/dashboard/managequeries"><i className="fa fa-address-book"></i> Manage Queries</Link></li>
                    <li><Link to="/dashboard/manageusers/user"><i className="fa fa-user-o"></i> Manage Users</Link></li>
                    <li><Link to="/dashboard/managelisting"><i className="fa fa-th-list"></i> Manage Listings</Link></li>
                    <li><Link to="/dashboard/manageusers/business"><i className="fa fa-user-o"></i> Manage Business</Link></li>
                    {//<li><Link to="/dashboard/managebusinessplan"><i className="fa fa-address-book"></i> Manage Business Plans</Link></li>
                    }
                    <li><Link onClick={this.logout}><i className="fa fa-power-off"></i> Logout</Link></li>
                </ul>	
                )
                case "business":
                return(
                    <ul>
                    <li className="active"><Link to="/dashboard" ><i className="fa fa-cogs"></i> Dashboard</Link></li>
                    <li><Link to="/dashboard/addnewlist"><i className="fa fa-list"></i> Add New Listings</Link></li>
                    <li><Link to="/dashboard/mylisting" data-toggle=""><i className="fa fa-th-list"></i> My Listings</Link></li>
                    <li><Link to="/dashboard/reviews"><i className="fa fa-star-o"></i> Reviews</Link></li>
                    <li><Link to="/dashboard/myprofile"><i className="fa fa-user-o"></i> My Profile</Link></li>
                    <li><Link to="/dashboard/changepassword"><i className="fa fa-user-o"></i> Change Password</Link></li>
                    <li><Link to="/dashboard/managequeryBusiness"><i className="fa fa-address-book"></i> Manage Queries</Link></li>
                    <li><Link onClick={this.logout}><i className="fa fa-power-off"></i> Logout</Link></li>
                </ul>	
                )
                case "user":
                return(
                    <ul>
                    <li className="active"><Link to="/dashboard" ><i className="fa fa-cogs"></i> Dashboard</Link></li>
                    <li><Link to="/dashboard/mylisting" data-toggle=""><i className="fa fa-th-list"></i> My Listings</Link></li>
                    <li><Link to="/dashboard/myprofile"><i className="fa fa-user-o"></i> My Profile</Link></li>
                    <li><Link to="/dashboard/changepassword"><i className="fa fa-user-o"></i> Change Password</Link></li>
                    <li><Link onClick={this.logout}><i className="fa fa-power-off"></i> Logout</Link></li>
                </ul>	
                )
                default:
                    break;
        }
         
       
    }
    componentWillMount(){
        this.setState({
            myProfile:JSON.parse(sessionStorage.getItem("loginState"))
        })
    }
    render(){
        
        return(
            <div id="dashboard-nav" className="dashboard-nav">	
                {this.returnMenu()}
	        </div>
        );
    }
}
export default Navbar;