import React, {Component} from 'react';
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import { loader } from "../../../../Redux/Actions";
import SimpleReactValidator from 'simple-react-validator';
import { connect } from "react-redux";
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../../CategorySlider/noImage.jpg';
import loaderImage from '../../../../Loader/abc.gif'
import ProfileService from '../../../../Services/ProfileService'
import UtilsService from '../../../../Services/UtilsService'
import swal from 'sweetalert';
import {storage} from '../../../../Firebase/index';
import { Link } from 'react-router-dom';
import '../dashboard.css';
import profileService from '../../../../Services/ProfileService';
class EditProfile extends Component{
    constructor(props){
        super(props)
        this.state={
            myProfile:null,
            myAddress:null,
            myContact:null,
            profileImg:null,
            file:null,
            firstName:'',
            lastName:'',
            addressLine1:'',
            addressLine2:'',
            area:'',
            city:'',
            state:'',
            country:'',
            pincode:'',
            phonenumber:'',
            email:'',
            worknumber:'',
        }
        this.validator2 = new SimpleReactValidator({autoForceUpdate: this});
        this.setValue=this.setValue.bind(this)
    }
    handleChange = event => {
		let change = {}
		change[event.target.name] = event.target.value
		this.setState(change)
  };
  setValue(){
      this.setState({
        firstName:this.state.myProfile.firstName,
        lastName:this.state.myProfile.lastName,
        addressLine1:this.state.myAddress.addressLine1,
        addressLine2:this.state.myAddress.addressLine2,
        area:this.state.myAddress.area,
        city:this.state.myAddress.city,
        state:this.state.myAddress.state,
        country:this.state.myAddress.country,
        pincode:this.state.myAddress.pinCode,
        phonenumber:this.state.myContact.phoneNumber,
        email:this.state.myContact.email,
        worknumber:this.state.myContact.workNumber,
      })
  }
    componentWillMount(){
        this.props.loader({show:true});
        let profile=JSON.parse(sessionStorage.getItem("loginState"))
        ProfileService.getProfileByProfileId(profile.id).then(
            (response) => {
                if (response.status === 200) {
  
                  this.setState({
                      myProfile:response.data.data
                  })
                  this.setState({
                      profileImg:this.state.myProfile.profileImage
                  })
                  console.log(this.state.myProfile)
                  UtilsService.getAddressById(this.state.myProfile.addressId).then(
                    (response) => {
                        if (response.status === 200) {
                          this.setState({
                              myAddress:response.data.data
                          })
                          console.log(this.state.myAddress)
                          UtilsService.getContactById(this.state.myProfile.contactId).then(
                            (response) => {
                                if (response.status === 200) {
                                  this.setState({
                                      myContact:response.data.data
                                  })
                                  console.log(this.state.myContact)
                                  this.setValue();
                                  this.props.loader({show:false});
                                }
                              },
                              (error) => {
                                console.log(error);
                                this.props.loader({show:false});
                              }
                            );
                          this.props.loader({show:false});
                        }
                      },
                      (error) => {
                        console.log(error);
                        this.props.loader({show:false});
                      }
                    );
                  this.props.loader({show:false});
                }
              },
              (error) => {
                console.log(error);
                this.props.loader({show:false});
              }
            );
        
    }
    triggerInputFile = () => this.fileInput.click();
    uploadFile = (event) =>{
        console.log(event.target.files[0])
        this.setState({
            file:event.target.files[0]
        })
    }

    uploadRequest = () =>{
        if(this.state.file===null){
            swal("error","Please Select Image First.","error")
        }else{

        
        this.props.loader({show:true});
        swal({
            title: "Are you sure?",
            text: "You Really Want To Change Your Profile Image?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willUpdate) => {
            if (willUpdate) {
                if(this.state.file!==null){      
                    console.log(this.state.file)
                    const uploadTask = storage.ref(`profile/${this.state.myProfile.profileId}/${this.state.file.name}`).put(this.state.file);
                    uploadTask.on('state_changed',(snapshot)=>{
                        this.setState({
                            progress:Math.round((snapshot.bytesTransferred/snapshot.totalBytes)*100)
                        })
            
                    }, (error)=>{
                        this.props.loader({show:false});
                        swal("error","Something went wrong. Please try again Later","error")
                    console.log(error);
                },()=>{
                    storage.ref(`profile/${this.state.myProfile.profileId}`).child(this.state.file.name).getDownloadURL().then(downloadurl=>{
                        this.setState({
                            profileImg:downloadurl
                        })
                        let data={
                            id:this.state.myProfile.profileId,
                            profileImage:this.state.profileImg+""
                        }
                        ProfileService.updateProfileImage(data).then(
                            (response) => {
                                if (response.status === 200) {
                                swal("Success","Your Profile Image Is updated Succesfully","success")
                                  this.props.loader({show:false});
                                }
                              },
                              (error) => {
                                console.log(error);
                                swal("error","Something went wrong. Please try again Later","error")
                                this.props.loader({show:false});
                              }
                            );
                        console.log(downloadurl)
                        this.props.loader({show:false});
                })})
                }else{
                    this.props.loader({show:false});
                }
            }else{
                this.props.loader({show:false});
            }})
        }
    }
    handleCancel =()=>{
        this.props.history.push("/dashboard/myprofile")
    }
    handleUpdate = (event) => {
        event.preventDefault();
        this.props.loader({show:true});

        if (this.validator2.allValid()) {
            swal({
                title: "Are you sure?",
                text: "You Really Want To Change Your Profile Image?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willUpdate) => {
                if (willUpdate) {
                    const data={
                        profile:{
                            id:this.state.myProfile.profileId,
                            userName:this.state.myProfile.userName,
                            profileActive:this.state.myProfile.profileActive,
                            firstName:this.state.firstName,
                            lastName:this.state.lastName,
                            role:this.state.myProfile.role
                        },
                        address:{
                            id:this.state.myAddress.addressId,
                            addressLine1:this.state.addressLine1,
                            addressLine2:this.state.addressLine2,
                            area:this.state.area,
                            city:this.state.city,
                            state:this.state.state,
                            country:this.state.country,
                            pinCode:this.state.pincode
                        },
                        contact:{
                            id:this.state.myContact.contactId,
                          email:this.state.email,
                          phoneNumber:this.state.phonenumber,
                          workNumber:this.state.worknumber
                        }
                    }
                    profileService.updateProfile(data.profile).then(
                      (response)=>{
                          
                          console.log(response)
                          UtilsService.updateAddress(data.address).then(
                            (response)=>{
                                console.log(response)
                                UtilsService.updateContact(data.contact).then(
                                    (response)=>{
                                        this.props.loader({show:false});
                                        swal("Update Sucessful","Your Profile Updated Succesfully.","success")
                                        this.props.history.push("/dashboard/myprofile")
                                        console.log(response)
                                    },
                                    (error)=>{
                                            this.props.loader({show:false});
                                            console.log(error);
                                            swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                                    }
                                  )
                            },
                            (error)=>{
                                    this.props.loader({show:false});
                                    console.log(error);
                                    swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                            }
                          )
                      },
                      (error)=>{
                              this.props.loader({show:false});
                              console.log(error);
                              swal("oops..", "It Seems Something Went Wrong. Please try again later in sometime..", "error");
                      }
                    )
                }else{
                    this.props.loader({show:false});
                }})
      }
        else{
            
          this.props.loader({show:false});
          this.validator2.showMessages();
          this.forceUpdate();
        }
    }

    setErrorImage = (e) => {
        e.target.src = noImage;
    }
    render(){
        let scrollPosition = 0;
        return(
            <div>
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
                <div id="titlebar">
			        <div className="row">
				        <div className="col-md-12">
					        <h2>Edit Profile</h2>
					
					        <nav id="breadcrumbs">
						        <ul>
							        <li><a href="#">Home</a></li>
							        <li><a href="#">Dashboard</a></li>
							        <li>Edit Profile</li>
						        </ul>
					        </nav>
				        </div>
			        </div>
		        </div>
        <div className="row">
			{this.state.myProfile!==null?<div className="col-lg-12 col-md-12">

            <div className="add_listing_info">
                            <h3>Profile Picture</h3>
                            <div className="row">			
                            <div class="user_image">
        <LazyLoadImage onClick={this.triggerInputFile} onError={this.setErrorImage.bind(this)} placeholderSrc={loaderImage} alt={'profile_image'} src={this.state.profileImg!==null?this.state.profileImg:"error.jpg"} scrollPosition={scrollPosition} effect="" height="150px" width="150px"/>
        </div>           
                            <input type="file" onChange={this.uploadFile} className="fileUpload" ref={fileInput => this.fileInput = fileInput}/>
                            <button onClick={this.uploadRequest} className="btn">Upload Image</button>
          </div>
                        </div>
			
            		<form onSubmit={this.handleUpdate}>
                    	<div className="add_listing_info">
                            <h3>Basic Informations</h3>				
                            <div className="form-group">
                                    <label className="label-title">First Name</label>
                                    <input type="text" className="form-control" placeholder="First Name" name="firstName" value={this.state.firstName} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('firstName') }></input>
                                    <div>
                                        <span className="has-error">{this.validator2.message('firstName', this.state.firstName, 'required|min:3')}</span>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="label-title">Last Name</label>
                                    <input type="text" className="form-control" placeholder="Last Name" name="lastName" value={this.state.lastName} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('lastName') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('lastName', this.state.lastName, 'required|min:3')}</span>
                                    </div>
                            </div>                   
                        </div> 
                        
                        <div className="add_listing_info">
                            <h3>Address Info</h3>				
                            <div className="form-group">
                                <label className="label-title">Address Line 1</label>
                                <input type="text" className="form-control" placeholder="Address Line 1" name="addressLine1" value={this.state.addressLine1} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('addressLine1') }></input>
                                <div>
                                     <span className="has-error">{this.validator2.message('addressLine1', this.state.addressLine1, 'required|min:3|max:80')}</span>
                                    </div>
                            </div>
                            <div className="form-group">
                                <label className="label-title">Address Line 2</label>
                                <input type="text" className="form-control" placeholder="Address Line 2" name="addressLine2" value={this.state.addressLine2} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('addressLine2') }></input>
                                <div>
                                     <span className="has-error">{this.validator2.message('addressLine2', this.state.addressLine2, 'min:3|max:80')}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-sm-6">
                                    <label className="label-title">Area</label>
                                    
                                    <input type="text" className="form-control" placeholder="Area" name="area" value={this.state.area} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('area') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('area', this.state.area, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>
                                <div className="form-group col-sm-6">
                                    <label className="label-title">City</label>
                                    <input type="text" className="form-control" placeholder="City" name="city" value={this.state.city} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('city') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('city', this.state.city, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-sm-6">
                                    <label className="label-title">State</label>
                                    <input type="text" className="form-control" placeholder="State" name="state" value={this.state.state} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('state') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('state', this.state.state, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>  
                                <div className="form-group col-sm-6">
                                    <label className="label-title">Country</label>
                                    <input type="text" className="form-control" placeholder="Country" name="country" value={this.state.country} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('country') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('country', this.state.country, 'required|min:3|max:10')}</span>
                                    </div>
                                </div>  
                                <div className="form-group col-sm-12">
                                    <label className="label-title">Pin Code</label>
                                    <input type="text" className="form-control" placeholder="Pin Code" name="pincode" value={this.state.pincode} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('pincode') }></input>
                                    <div>
                                     <span className="has-error">{this.validator2.message('pincode', this.state.pincode, 'required|min:6|max:6|numeric')}</span>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div className="add_listing_info">
                            <h3>Contact Info</h3>
                           
                            <div className="form-group">
                                    <label className="label-title">Phone Number</label>
                                    <input type="text" className="form-control" placeholder="Phone Number" name="phonenumber" value={this.state.phonenumber} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('phonenumber') }></input>
                                    <div>
                <span className="has-error">{this.validator2.message('phonenumber', this.state.phonenumber, 'required|min:10|max:10|phone')}</span>
            </div>
                                </div>
	                            <div className="form-group">
                                    <label className="label-title">Email</label>
                                    <input type="text" className="form-control" placeholder="Email" name="email" value={this.state.email} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('email') }></input>
                                    <div>
                <span className="has-error">{this.validator2.message('email', this.state.email, 'required|email')}</span>
            </div>
                                </div>
                                <div className="form-group">
                                    <label className="label-title">Work Number</label>
                                    <input type="text" className="form-control" placeholder="Work Number" name="worknumber" value={this.state.worknumber} onChange={this.handleChange} onBlur={() => this.validator2.showMessageFor('worknumber')}></input>
                                    <div>
                <span className="has-error">{this.validator2.message('worknumber', this.state.worknumber, 'min:10|max:10|phone')}</span>
            
                                </div>
                                </div>
                                      
                        </div>   
                        <div className="add_listing_info">
                            <button className="btn" onClick={this.handleUpdate}>Update Profile</button>&nbsp;
                            &nbsp;
                            <button className="btn" onClick={this.handleCancel}>Cancel</button>
                        </div>   
                    </form>
			</div>: <div className="add_listing_info">
                            <h3>No Profile Found. Something went wrong please try again later.</h3>				
                                              
                        </div>}


			
			</div>
		</div>
        <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
			
            </div>
        </div>
            </div>
            
        )
    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}
export default connect(null, mapDispatchToProps)(trackWindowScroll(EditProfile));