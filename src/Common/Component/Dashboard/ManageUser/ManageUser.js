import React, {Component} from 'react'
import DashBoardHeader from '../Header/DashBoardHeader'
import Navbar from '../Navbar/Navbar'
import '../dashboard.css';
import { loader } from "../../../../Redux/Actions";
import { connect } from "react-redux";
import swal from 'sweetalert';
import profileService from '../../../../Services/ProfileService';
import { Redirect } from 'react-router-dom';
class ManageUser extends Component{
    constructor(props){
        super(props)
        this.state={
            allProfile:null
        }
        this.isBusiness=false
        this.handleActive=this.handleActive.bind(this)
    }

    componentWillMount(){
        this.isBusiness=window.location.href.includes("business")
    }
    
    componentDidMount(){
        this.props.loader({show:true})
        profileService.getProfileByRole(this.isBusiness?"business":"user").then(
            (response) => {
                this.props.loader({show:false})
                if (response.status === 200) {
                    this.setState({
                        allProfile: response.data.data
                    })
                }
            },
            (error) => {
                this.props.loader({show:false});
                swal("Oops.", "Sorry We Coudn't able to find any profile for you right now please try again later..", "error");
                console.log(error)
                throw error;
            }
        )
    }
    handleActive(event){
        this.props.loader({show:true});
        let param={
            profileId:event.target.name,
            status:event.target.value==="true"?false:true
        }
        swal({
            title: "Are you sure?",
            text: "You Really Want To Change The Status Of This User",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willUpdate) => {
            if (willUpdate) {
                profileService.updateStatus(param).then(
                    (response)=>{
                        this.props.loader({show:false});
                        if(response.status===200)
                        {
                            swal("Sucess! Users Status Changed Succesfully!", {
                                icon: "success",
                              });
                            this.componentDidMount()
                        }
                    },
                    (error)=>{

                        this.props.loader({show:false});
                        swal("OOPS! Something Went Wrong Please Try Again Later!", {
                            icon: "error",
                          });
                        this.componentDidMount()
                        throw error;
                    }
                )
              }else{
                this.props.loader({show:false})
              }
          });
    }
    renderRedirect = () => {
        this.user=JSON.parse(sessionStorage.getItem("loginState"))
        if (this.user.role!=="admin") {
          return <Redirect to='/dashboard' />
        }
      }
    render(){
        return(
            <div>
                {this.renderRedirect()}
                <div>
                <DashBoardHeader/>
                <br></br>
                <br></br>
                <div className="dashboard">
                        <Navbar/>
            <div className="dashboard-content">
            <div id="titlebar">
                <div className="row">
                    <div className="col-md-12">
                            <h2>Manage {this.isBusiness?"Business":"Users"}</h2>
                
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Dashboard</a></li>
                                <li>Manage {this.isBusiness?"Business":"Users"}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="row">
            <div className="col-lg-12 col-md-12">
            {this.state.allProfile===null?
                <div className="add_listing_info">
                    <h3>Sorry We Coundn't able to find any profile for you right now.</h3>
                </div>:
                <div className="add_listing_info">
                 <h3>{this.isBusiness?"Business":"Users"}</h3>	
                 <table className="table table-hover table-responsive">
       <thead>
         <tr>
           <th scope="col"># Profile ID</th>
           <th scope="col">Full Name</th>
           <th scope="col">Username</th>
           <th scope="col">Email</th>
           <th scope="col">Phone Number</th>
           <th scope="col">Action</th>
         </tr>
       </thead>
       <tbody>
       {this.state.allProfile.map((rowData) => (
                 <tr key={rowData.profileId}>
                     <th scope="row">{rowData.profileId}</th>
                        <td>{rowData.firstName} {rowData.lastName}</td>
                        <td>{rowData.userName}</td>
                        <td>{rowData.email}</td>
                        <td>{rowData.phone}</td>
                        <td><button value={rowData.profileActive} name={rowData.profileId} onClick={this.handleActive} className={rowData.profileActive?"btn green":"btn red"}>{rowData.profileActive?"Active":"InActive"}</button></td>
                       
                 </tr>

))}
        
       </tbody>
     </table>
     </div>}
                
                </div>
                </div>
            </div>
        </div>
        <br></br> 
				<div className="copyrights">Copyright &copy; 2020 TownHub. All Rights Reserved</div>
        </div>
        </div>
        </div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        loader:(payload)=>{
            dispatch(loader(payload));
        }
    }
}


export default connect(null, mapDispatchToProps)(ManageUser);