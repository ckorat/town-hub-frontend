import React, {Component} from 'react';
import logo from './logo.png';
import {Link} from 'react-router-dom';
import swal from 'sweetalert';
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
import noImage from '../../CategorySlider/noImage.jpg';
import loaderImage from '../../../../Loader/abc.gif'
import ProfileService from '../../../../Services/ProfileService'
class DashBoardHeader extends Component{
    constructor(props){
        super(props);
        this.state={
            isShow:false,
            myProfile:null
        }
        this.handleDropdown=this.handleDropdown.bind(this);
        this.logout=this.logout.bind(this)
    }
    handleDropdown(){
        this.setState({
            isShow:!this.state.isShow
        })
        }
    logout(){
            sessionStorage.clear()
            swal("You're Logged Out.", "Thank You For Using Our Services. We love to see you again soon..", "success");
    }
    componentWillMount(){
        let profile=JSON.parse(sessionStorage.getItem("loginState"))
        ProfileService.getProfileByProfileId(profile.id).then(
            (response) => {
                if (response.status === 200) {
  
                  this.setState({
                      myProfile:response.data.data
                  })
                  console.log(this.state.myProfile)
                }
              },
              (error) => {
                console.log(error);
              }
            );
        
    }
    setErrorImage = (e) => {
        console.log("error")
        e.target.src = noImage;
    }
    render(){
        let scrollPosition = 0;
        return(
            
            <header id="header">
        <nav id="fixedTop" className="navbar navbar-expand-lg fixed-top">
            <div className="container-fluid">
               <div className="navbar-header">
                <div className="logo"> <Link to="/"><img src={logo} alt="image"/></Link> </div>
               </div>
                <div className="collapse navbar-collapse" id="">
                    <div className="user_nav ml-auto">
                        <div className="dropdown">
                          <span id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <LazyLoadImage onClick={this.handleDropdown} onError={this.setErrorImage.bind(this)} placeholderSrc={loaderImage} alt={'profile_image'} src={this.state.myProfile?.profileImage!==null?this.state.myProfile?.profileImage:"error.jpg"} scrollPosition={scrollPosition} effect="" /> 
                          </span>
                          <ul className={this.state.isShow?"dropdown-menu show":"dropdown-menu"} aria-labelledby="dLabel">
                            <li><Link to="/dashboard"><i className="fa fa-cogs"></i> Dashboard</Link></li>
                            <li><Link to="/dashboard/myprofile"><i className="fa fa-user-o"></i> My Profile</Link></li>
                            <li onClick={this.logout}><Link><i className="fa fa-power-off"></i> Logout</Link></li>                   
                          </ul>
                        </div>
                    </div>
                    <div className="submit_listing">
                        <Link to="/dashboard/addnewlist" className="btn outline-btn"><i className="fa  fa-plus-circle"></i> Submit Listing</Link>
                     </div>
                </div>
             </div>   
        </nav>
    </header>
        );
    }
}
export default trackWindowScroll(DashBoardHeader)