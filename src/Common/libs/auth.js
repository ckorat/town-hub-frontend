class Auth {
    constructor() {
      this.authenticated = false
    }
    isAuthenticated() {
        if(sessionStorage.getItem("loginState")){
            return this.authenticated=true
        }else{
            return this.authenticated=false
        }
    }
  }
  
  export default new Auth();
  