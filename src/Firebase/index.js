import firebase from '../../node_modules/firebase/app';
import '../../node_modules/firebase/storage';
var firebaseConfig = {
    apiKey: "AIzaSyDFni5reTmda4BbJ8PuvwYf4ofFAXcz2-w",
    authDomain: "react-image-3bd09.firebaseapp.com",
    databaseURL: "https://react-image-3bd09.firebaseio.com",
    projectId: "react-image-3bd09",
    storageBucket: "react-image-3bd09.appspot.com",
    messagingSenderId: "178489751262",
    appId: "1:178489751262:web:d664265dd1837b778db44f",
    measurementId: "G-0TMSJDYX71"
  };
  firebase.initializeApp(firebaseConfig);
  const storage = firebase.storage();
  
  export {
      storage, firebase as default
  }